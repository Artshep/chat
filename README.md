Run make script to start the docker. 
Technologies: 
Front: React, Next (the most recent version - next) 
Back: Express, Fastify (the most recent version - fastify) 
DB: Mongo DB UI framework: 
Material UI 

Application config: 
Authorization using JWT and oAuth (google, facebook); 
Invitation - via e-mail by admin; Admin may block / unblock user, invite / resend invites to new users; 
User has profile page to edit his info; 
Chat based on websockets (socket.io); Fully responsive design; Media stores on CDN server;