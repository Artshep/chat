import React, { Component } from "react";
import PrivateRoute from "../src/components/HOC/PrivateRoute";
import Profile from "../src/pages/profile/Profile";

export default class ProfilePage extends Component {
  render() {
    return <PrivateRoute component={Profile} />;
  }
}
