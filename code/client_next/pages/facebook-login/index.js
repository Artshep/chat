import React, { Component } from "react";
import PublicRoute from "../../src/components/HOC/PublicRoute";
import OauthLogin from "../../src/pages/oauthLogin/OauthLogin";

export default class oauthLoginPage extends Component {
  render() {
    return <PublicRoute component={OauthLogin} />;
  }
}
