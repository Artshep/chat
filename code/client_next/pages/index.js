import React, { Component } from "react";
import PrivateRoute from "../src/components/HOC/PrivateRoute";
import Home from "../src/pages/home/Home.js";

export default class HomePage extends Component {
  render() {
    return <PrivateRoute component={Home} />;
  }
}
