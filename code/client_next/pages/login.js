import React from "react";
import PublicRoute from "../src/components/HOC/PublicRoute";
import Login from "../src/pages/login/Login";

export default function LoginPage() {
  return <PublicRoute component={Login} />;
}
