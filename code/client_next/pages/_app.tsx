import React, { useState } from "react";
import Head from "next/head";
import MyContext from "../src/components/MyContext";
import SnackAlert from "../src/components/snackAlert/SnackAlert";

import CssBaseline from "@material-ui/core/CssBaseline";
// import theme from '../src/theme';

interface IOnChange {
    message: string,
    open: boolean,
    type: string
}

export default function MyApp(props) {
  const { Component, pageProps } = props;
  const [open, ToggleOpen] = useState(false);
  const [type, setType] = useState("success");
  const [message, setMessage] = useState("");

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  function onChangeOpen(result:IOnChange) {
    ToggleOpen(result.open);
    setType(result.type);
    setMessage(result.message);
  }

  return (
    <>
      <MyContext.Provider value={{ onChangeOpen: onChangeOpen }}>
        <SnackAlert
          open={open}
          onChangeOpen={onChangeOpen}
          type={type}
          message={message}
        />
        <Head>
          <title>My page</title>
          <meta
            name="viewport"
            content="minimum-scale=1, initial-scale=1, width=device-width"
          />
        </Head>
        {/* <ThemeProvider theme={theme}> */}
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Component {...pageProps} />
        {/* </ThemeProvider> */}
      </MyContext.Provider>
    </>
  );
}
