import React, { Component } from "react";
import PublicRoute from "../../src/components/HOC/PublicRoute";
import CompleteRecover from "../../src/pages/completeRecover/CompleteRecover";

export default class completeRecoverPage extends Component {
  render() {
    return <PublicRoute component={CompleteRecover} />;
  }
}
