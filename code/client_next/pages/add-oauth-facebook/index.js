import React, { Component } from "react";
import PrivateRoute from "../../src/components/HOC/PrivateRoute";
import OauthRedirect from "../../src/pages/oauthRedirect/OauthRedirect";

export default class OauthRedirectPage extends Component {
  render() {
    return <PrivateRoute component={OauthRedirect} />;
  }
}
