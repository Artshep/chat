import React, { Component } from "react";
import PrivateRoute from "../src/components/HOC/PrivateRoute";
import Users from "../src/pages/users/Users";

export default class UsersPage extends Component {
  render() {
    return <PrivateRoute component={Users} />;
  }
}
