import React, { Component } from "react";
import PrivateRoute from "../src/components/HOC/PrivateRoute";
import Invite from "../src/pages/invite/Invite";

export default class InvitePage extends Component {
  render() {
    return <PrivateRoute component={Invite} />;
  }
}
