import React, { Component } from "react";
import PublicRoute from "../src/components/HOC/PublicRoute";
import Forget from "../src/pages/forget/Forget";

export default class ForgetPage extends Component {
  render() {
    return <PublicRoute component={Forget} />;
  }
}
