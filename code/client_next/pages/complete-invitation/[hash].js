import React, { Component } from "react";
import PublicRoute from "../../src/components/HOC/PublicRoute";
import CompleteInvitation from "../../src/pages/completeInvitation/CompleteInvitation";

export default class completeInvitationPage extends Component {
  render() {
    return <PublicRoute component={CompleteInvitation} />;
  }
}
