interface IData {
  name: string
  email: string
  role: string
  oauth: IOauth
  avatar: string
  _id: string
}
interface IOauth {
  google: boolean
  facebook: boolean
}

export default function parseJwt(token: string): IData {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
  return JSON.parse(jsonPayload);
}
