const config = {
  googleRedirectLink: process.env.NEXT_PUBLIC_GOOGLE_REDIRECT,
  googleLoginLink: process.env.NEXT_PUBLIC_GOOGLE_LOGIN,
  facebookRedirectLink: process.env.NEXT_PUBLIC_FACEBOOK_REDIRECT,
  facebookLoginLink: process.env.NEXT_PUBLIC_FACEBOOK_LOGIN,
};
export default config;
