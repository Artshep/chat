import React, { useState, useEffect } from "react";
import Header from "../header/Header";
import parseJwt from "../../utils/parseJWT";
import { useRouter } from "next/router";

function PrivateRoute(props) {
  const router = useRouter();
  const [user, setUser] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("jwt");
    if (!token) return router.push("/login");

    setUser(parseJwt(localStorage.getItem("jwt")));
  }, []);

  return !user ? (
    <></>
  ) : (
    <>
      <Header user={user} />
      <props.component user={user} />
    </>
  );
}

export default PrivateRoute;
