import React, { useEffect } from "react";
import Router from "next/router";

function PublicRoute(props) {
  useEffect(() => {
    const token = localStorage.getItem("jwt");

    if (token) return Router.push("/");
  }, []);
  return <props.component />;
}

export default PublicRoute;
