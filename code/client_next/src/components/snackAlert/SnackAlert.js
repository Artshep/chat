import MuiAlert from "@material-ui/lab/Alert";
import { Snackbar } from "@material-ui/core";
import React from "react";

export default function SnackAlert(props) {
  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      open={props.open}
      transitionDuration={500}
      autoHideDuration={3000}
      onClose={() => props.onChangeOpen({ ...props, open: false })}
    >
      <MuiAlert elevation={6} variant="filled" severity={props.type}>
        {props.message}
      </MuiAlert>
    </Snackbar>
  );
}
