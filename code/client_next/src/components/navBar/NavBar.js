import React from "react";
import {
  List,
  Drawer,
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  makeStyles,
  Box,
} from "@material-ui/core";
import {
  PersonAdd,
  ExitToApp,
  People,
  AccountCircle,
} from "@material-ui/icons";
import Link from "next/link";
import { useRouter } from "next/router";

const useStyles = makeStyles(() => ({
  link: {
    color: "black",
  },
  icon: {
    color: "black",
  },
  linkComponent: {
    textDecoration: "none",
  },
}));

function NavBar(props) {
  const router = useRouter();
  const classes = useStyles();

  function onLogout() {
    localStorage.removeItem("jwt");
    router.push("/login");
  }

  const list = [
    {
      link: "/profile",
      icon: <AccountCircle />,
      text: "Profile",
    },
    {
      link: "/users",
      icon: <People />,
      text: "Users List",
      role: "admin",
    },
    {
      link: "/invite",
      icon: <PersonAdd />,
      text: "Invite new user",
      role: "admin",
    },
  ];

  let menuList =
    props.role === "admin"
      ? list
      : list.filter((item) => item.role !== "admin");

  return (
    <Drawer
      open={props.open}
      onClick={() => props.onOpen(!props.open)}
      anchor={"left"}
      transitionDuration={300}
      style={{
        width: 250,
      }}
    >
      <List style={{ width: 300 }}>
        {menuList.map((item, i) => {
          return (
            <Box key={item.text}>
              <Link href={item.link} className={classes.linkComponent}>
                <ListItem button>
                  <ListItemIcon className={classes.icon}>
                    {item.icon}
                  </ListItemIcon>
                  <ListItemText className={classes.link} primary={item.text} />
                </ListItem>
              </Link>
            </Box>
          );
        })}

        <Divider />
        <ListItem button onClick={onLogout}>
          <ListItemIcon className={classes.icon}>
            <ExitToApp />
          </ListItemIcon>
          <ListItemText primary={"Logout"} />
        </ListItem>
      </List>
    </Drawer>
  );
}

export default NavBar;
