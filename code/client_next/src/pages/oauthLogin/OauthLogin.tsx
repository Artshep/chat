import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import { withRouter } from "next/router";

class OauthLogin extends Component<{router}, any> {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
    };
  }
  componentDidMount() {
    let apiUrl = window.location.pathname.includes("google")
      ? "/api/google-login"
      : "/api/facebook-login";
    let params = new URLSearchParams(window.location.search);
    let code = params.get("code");
    axiosReq
      .oauthLogin(apiUrl, code)
      .then((res) => {
        localStorage.setItem("jwt", res);
        this.setState({ signedIn: true });
      })
      .then((res) => this.props.router.push("/"))
      .catch((e) => this.props.router.push("/login"));
  }
  render() {
    return <></>;
  }
}

export default withRouter(OauthLogin);
