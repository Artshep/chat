import React, { useState, useEffect } from "react";
import axiosReq from "../../api/axiosReq";
import { useRouter } from "next/router";
import CompleteInvitationPage from "./CompleteInvitationPage";

const CompleteInvitation = () => {
  const router = useRouter();
  const [password, setPassword] = useState();
  const [email, setEmail] = useState("");
  const [isValidHash, toggleValidHash] = useState(false);
  const [isValidPassword, toggleValidPassword] = useState(true);

  useEffect(() => {
    console.log(window.location.pathname);
    axiosReq
      .checkUrl(window.location.pathname, "/api/check-invitation")
      .then((res) => {
        setEmail(res.email);
        toggleValidHash(true);
      })
      .catch((e) => {
        toggleValidHash(false);
        return router.push("/login");
      });
  }, []);

  const handleChangeInput = (e) => {
    setPassword(e.target.value);
    e.target.value.length < 6
      ? toggleValidPassword(false)
      : toggleValidPassword(true);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axiosReq
      .setPassword(email, password, "/api/complete-invitation")
      .then((res) => {
        localStorage.setItem("jwt", res.token);
        router.push("/");
      });
  };

  return !isValidHash ? (
    <></>
  ) : (
    <CompleteInvitationPage
      handleSubmit={handleSubmit}
      handleChangeInput={handleChangeInput}
      validPassword={isValidPassword}
    />
  );
};

export default CompleteInvitation;
