import React, { Component } from "react";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TabPanelFile from "./TabPanelFile";
import { Box, List } from "@material-ui/core";
import Message from "./pinnedMessages/Message";
import Files from "./files/Files";

export default class FileInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      messages: null,
      messageList: null,
    };
    this.changeTable = this.changeTable.bind(this);
  }
  changeTable() {
    this.setState({ value: this.state.value === 0 ? 1 : 0 });
  }

  componentDidUpdate(prevProps, prevState) {
    this.createMessageList(prevProps);
  }

  createMessageList(prevProps) {
    if (prevProps && this.props.messages !== prevProps.messages) {
      let messageList = this.props.messages.map((message) => {
        return <Message message={message} key={message.id} />;
      });
      this.setState({ messageList: messageList });
    }
  }
  render() {
    return (
      <Box>
        <Box>
          <Tabs
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            aria-label="action tabs example"
            value={this.state.value}
            onChange={this.changeTable}
          >
            <Tab label="File" />
            <Tab label="Pinned Messages" />
          </Tabs>
          <TabPanelFile value={this.state.value} index={0}>
            <Files />
          </TabPanelFile>
          <TabPanelFile value={this.state.value} index={1}>
            <List
              style={{
                width: "100%",
                maxHeight: "calc(100vh - 64px - 96px)",
                overflow: "auto",
              }}
            >
              {this.state.messageList}
            </List>
          </TabPanelFile>
        </Box>
      </Box>
    );
  }
}
