import React, { useState } from "react";
import FolderOpenRoundedIcon from "@material-ui/icons/FolderOpenRounded";
import DescriptionIcon from "@material-ui/icons/Description";
import { Box, Typography, IconButton } from "@material-ui/core";
import FileMenu from "./FileMenu";

const FilesDir = (props) => {
  const [displayMenu, toggleMenu] = useState(false);

  let files = [];
  let path = props.path.join("/");

  const contextMenuHandler = (e) => {
    e.preventDefault(e);
    toggleMenu(!displayMenu);
  };

  for (let key in props.filesList) {
    let file;
    if (props.filesList[key] === "dir") {
      file = (
        <Box
          display="flex"
          justifyContent="center"
          textAlign="center"
          width="80px"
          maxHeight="80px"
          key={key}
          pr={1}
        >
          <FileMenu
            onDeleteFile={props.onDeleteFile}
            path={path}
            filename={key}
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() => props.setPath(key)}
            >
              <FolderOpenRoundedIcon />
            </IconButton>
            <Typography variant="subtitle2">{key}</Typography>
          </FileMenu>
        </Box>
      );
    } else {
      file = (
        <Box
          display="flex"
          width="80px"
          maxHeight="80px"
          textAlign="center"
          key={key}
          pr={1}
        >
          <FileMenu
            onDeleteFile={props.onDeleteFile}
            path={path}
            filename={key}
          >
            <a
              href={`http://localhost:4000/front/files/${path}/${key}`}
              download
            >
              <IconButton size="small" color="primary">
                <DescriptionIcon onContextMenu={contextMenuHandler} />
              </IconButton>
            </a>
            <Typography variant="subtitle2">{key}</Typography>
          </FileMenu>
        </Box>
      );
    }
    files.push(file);
  }

  return (
    <Box display="flex" width="100%" flexWrap="wrap" alignItems='flex-start'>
      {files || null}
    </Box>
  );
};
export default FilesDir;
