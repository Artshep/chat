import React, { Component } from "react";
import { Box, Grid, Hidden } from "@material-ui/core";
import Video from "./video/Video";
import Chat from "./chat/Chat";
import FileInfo from "./fileInfo/FileInfo";
import io from "socket.io-client";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.socket = io.connect("http://localhost:5000", {
      query: { token: localStorage.getItem("jwt") },
    });
    this.google = localStorage.getItem("google");
    this.facebook = localStorage.getItem("facebook");
    this.state = {
      pinnedMessages: [],
    };
  }

  componentDidMount() {
    this.socket.on("displayPinnedMessages", (messages) => {
      this.setState({ pinnedMessages: messages });
    });
  }

  render() {
    return (
      <Grid container direction="row">
        <Grid item xs={12} md={7}>
          <Box display="flex" flexDirection="column" width="100%">
            <Box height={"calc((100vh - 64px) * 0.6)"}>
              <Video />
            </Box>
            <Hidden smDown>
              <Box height={"calc((100vh - 64px) * 0.4)"}>
                <FileInfo messages={this.state.pinnedMessages} />
              </Box>
            </Hidden>
          </Box>
        </Grid>
        <Grid item xs={12} md={5}>
          <Box
            display="flex"
            flexDirection="column"
            boxSizing="border-box"
            width="100%"
            height={"calc(100vh - 64px)"}
          >
            <Chat user={this.props.user} socket={this.socket} />
          </Box>
        </Grid>
      </Grid>
    );
  }
}
