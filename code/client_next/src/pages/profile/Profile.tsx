import React, { Component } from "react";
import MyContext from "../../components/MyContext";
import ProfilePage from "./ProfilePage";
import parseJWT from "../../utils/parseJWT";

import axiosReq from "../../api/axiosReq";
import axios from "axios";

interface IState {
  name: string
  avatar: string
  email: string
  role: string
  google: boolean
  facebook: boolean
  changedName: string
}

export default class Profile extends Component<any, IState> {
  static contextType = MyContext;
  constructor(props) {
    super(props);

    this.changeAvatar = this.changeAvatar.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleForm = this.handleForm.bind(this);
    this.state = {
      name: '',
      avatar: '',
      email: '',
      role: '',
      changedName: '',
      google: null,
      facebook: null
    };
  }

  componentDidMount() {
    const token = parseJWT(localStorage.getItem("jwt"));
    this.setState({
      avatar: token.avatar,
      name: token.name,
      changedName: token.name,
      email: token.email,
      role: token.role,
      google: token.oauth.google,
      facebook: token.oauth.facebook,
    });
  }

  changeAvatar(e) {
    const formData = new FormData();
    formData.append("avatar", e.target.files[0]);
    axios
      .post("/api/update-user-avatar", formData, {
        baseURL: "http://localhost:5000",
        headers: {
          "Content-Type": "multipart/form-data",
          "x-authorization": localStorage.getItem("jwt"),
        },
      })
      .then((res) => {
        localStorage.setItem("jwt", res.data);
        const token = parseJWT(res.data);
        this.setState({ avatar: token.avatar });
      });
  }

  handleInput(e) {
    if (e.target.name === "name") {
      this.setState({ name: e.target.value });
    }
  }
  handleForm(e) {
    e.preventDefault();
    axiosReq.changeName(this.state.name).then((res) => {
      localStorage.setItem("jwt", res);
      const token = parseJWT(res);
      this.setState({ changedName: token.name });
      this.context.onChangeOpen({
        message: "Name has been changed!",
        open: true,
        type: "success",
      });
    });
  }

  render() {
    return (
      <>
        <ProfilePage
          avatar={this.state.avatar}
          changeAvatar={this.changeAvatar}
          changedName={this.state.changedName}
          email={this.state.email}
          role={this.state.role}
          handleInput={this.handleInput}
          handleForm={this.handleForm}
          google={this.state.google}
          facebook={this.state.facebook}
        />
      </>
    );
  }
}
