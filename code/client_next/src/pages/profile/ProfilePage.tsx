import React from "react";
import config from "../../config/next.config";
import {
  Avatar,
  TextField,
  Button,
  Card,
  CardContent,
  Box,
  Grid,
} from "@material-ui/core";

export default function ProfilePage(props) {
  const {
    avatar,
    changeAvatar,
    changedName,
    email,
    role,
    handleInput,
    handleForm,
    google,
    facebook
  } = props;

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      paddingTop="30px"
    >
      <Card>
        <CardContent style={{ minWidth: "600px" }}>
          <Box display="flex">
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              justifyContent="center"
            >
              <Avatar
                src={`http://localhost:4000/${avatar}`}
                alt={"avatar"}
                style={{
                  width: "150px",
                  height: "150px",
                  border: "1px solid #d9d7d7",
                  boxShadow: "1px 1px 3px 1px #000000",
                  marginBottom: "20px",
                }}
              />

              <Button component="label" variant="outlined" color={"primary"}>
                <input type="file" onChange={changeAvatar} hidden />
                Change avatar
              </Button>
            </Box>
            <Box
              width="100%"
              display="flex"
              flexDirection="column"
              padding="20px"
            >
              <Box>Email: {email}</Box>
              <Box>Role: {role}</Box>
              <Box pb={2}>Nickname: {changedName}</Box>
              <Grid container>
                {!google && (
                  <Grid item md={5} xs={12}>
                    <Button
                      variant="contained"
                      color="primary"
                      href={config.googleRedirectLink}
                    >
                      LINK GOOGLE
                    </Button>
                  </Grid>
                )}
                {!facebook && (
                  <Grid item md={5}>
                    <Button
                      variant="contained"
                      color="primary"
                      href={config.facebookRedirectLink}
                    >
                      LINK FACEBOOK
                    </Button>
                  </Grid>
                )}
              </Grid>
            </Box>
          </Box>

          <Box paddingTop={1}>
            Change name:
            <form method="POST" onSubmit={handleForm}>
              <Box display="flex" alignItems="center">
                <TextField
                  size="small"
                  variant="outlined"
                  margin="normal"
                  name="name"
                  label="Name"
                  type="name"
                  id="name"
                  onChange={handleInput}
                />

                <Box paddingLeft={1} marginTop="7px">
                  <Button variant="contained" color="primary" type="submit">
                    Change
                  </Button>
                </Box>
              </Box>
            </form>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
}
