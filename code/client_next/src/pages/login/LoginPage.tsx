import React from "react";
import {
  Button,
  Grid,
  TextField,
  Box,
  Container,
  Typography,
  Card,
} from "@material-ui/core";
import config from "../../config/next.config";

export default function LoginPage(props) {
  return (
    <Container maxWidth="sm">
      <Box
        height="80vh"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Card>
          <Box p={4}>
            <form method="POST" onSubmit={props.handleSubmit}>
              <Box textAlign="center">
                <Typography variant="subtitle2">SIGN IN</Typography>
              </Box>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                defaultValue={props.email}
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={props.handleInput}
              />
              <TextField
                error={!props.validPassword}
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={props.handleInput}
              />
              <Box
                display="flex"
                flexDirection="row-reverse"
                marginBottom="5px"
              >
                <a href="forget">Forget password?</a>
              </Box>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                fullWidth
                disabled={!props.validPassword}
              >
                Login
              </Button>

              <Box marginTop="10px">
                <Grid container spacing={1}>
                  <Grid item xs={6}>
                    <Button
                      href={config.googleLoginLink}
                      variant="contained"
                      color="primary"
                      fullWidth
                    >
                      Log In With Google
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      href={config.facebookLoginLink}
                      variant="contained"
                      color="primary"
                      fullWidth
                    >
                      Log in with Facebook
                    </Button>
                  </Grid>
                </Grid>
              </Box>
            </form>
          </Box>
        </Card>
      </Box>
    </Container>
  );
}
