import React, { useState, useContext } from "react";
import { useRouter } from "next/router";
import LoginPage from "./LoginPage";
import axiosReq from "../../api/axiosReq";
import MyContext from "../../components/MyContext";

export default function Login() {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [validPassword, validatePassword] = useState(true);
  const onChangeOpen = useContext(MyContext);

  const handleSubmit = (e:React.SyntheticEvent): void => {
    e.preventDefault();
    axiosReq
      .login(email, password)
      .then((res: string) => {
        localStorage.setItem("jwt", res);
        router.push("/");
      })
      .catch((e): void => {
        onChangeOpen.onChangeOpen({
          message: e.response.data,
          open: true,
          type: "error",
        });
      });
  };

  const handleInput = (e:React.SyntheticEvent) => {
    const { name, value } = e.target as HTMLInputElement;
    if (name === "email") {
      setEmail(value);
    } else {
      setPassword(value);
      value.length < 6
        ? validatePassword(false)
        : validatePassword(true);
    }
  };

  return (
    <LoginPage
      handleInput={handleInput}
      handleSubmit={handleSubmit}
      email={email}
      validPassword={validPassword}
    />
  );
}
