import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import {
  Button,
  Avatar,
  Container,
  Box,
  Table,
  TableContainer,
  TableBody,
  TableCell,
  TableRow,
} from "@material-ui/core";

export default class Users extends Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {};
  }

  getUsersCollection(collection:string): void {
    axiosReq.getCollection(collection).then((res) => {
      this.setState({ users: res.users });
    });
  }

  componentDidMount() {
    this.getUsersCollection("users");
  }

  changeStatus(email: string): void {
    axiosReq
      .changeStatus(email)
      .then((res) => this.getUsersCollection("users"));
  }

  render() {
    return (
      <Container maxWidth="md">
        <Box m={5}>
          <TableContainer>
            <Table size="small" aria-label="a dense table">
              <TableBody>
                {this.state.users &&
                  this.state.users.map((user) => (
                    <TableRow key={user._id}>
                      <TableCell align="right">
                        <Avatar
                          alt="user"
                          src={`http://localhost:4000/${user.avatar}`}
                        />
                      </TableCell>
                      <TableCell align="right">{user.name}</TableCell>
                      <TableCell align="right">{user.email}</TableCell>
                      <TableCell align="right">
                        {user.status === "active" ? (
                          <Button
                            variant="contained"
                            color="primary"
                            onClick={(e) => this.changeStatus(user.email)}
                          >
                            Active
                          </Button>
                        ) : (
                          <Button
                            variant="contained"
                            color="secondary"
                            onClick={(e) => this.changeStatus(user.email)}
                          >
                            Blocked
                          </Button>
                        )}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Container>
    );
  }
}
