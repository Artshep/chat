import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import MyContext from "../../components/MyContext";
import InvitePage from "./InvitePage";

interface IUsers {
    name: string;
    email: string;
    avatar: string
    role: string
    status: string
    _id: string
  }
interface IState {
    inviteEmail: string,
    users: IUsers[],
}

export default class Invite extends Component<any, IState> {
  static contextType = MyContext;
  constructor(props) {
    super(props);

    this.resendInvite = this.resendInvite.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleInviteForm = this.handleInviteForm.bind(this);
    this.state = {
        users: [],
        inviteEmail: ''
    };
  }

  handleInput(e) {
    const { value } = e.target as HTMLInputElement;
    this.setState({
      inviteEmail: value,
    });
  }

  resendInvite(email) {
    axiosReq.invite(email).then((res) => {
      this.context.onChangeOpen({
        message: "Invitation has been resent!",
        open: true,
        type: "success",
      });
    });
  }

  getCollection() {
    axiosReq
      .getCollection("invitations")
      .then((res) => this.setState({ users: res.users }));
  }

  handleInviteForm(e) {
    e.preventDefault();
    axiosReq
      .invite(this.state.inviteEmail)
      .then((res) => {
        this.context.onChangeOpen({
          message: "User has been invited!",
          open: true,
          type: "success",
        });
        this.getCollection();
      })
      .catch((e) => {
        this.context.onChangeOpen({
          message: "User is already invited",
          open: true,
          type: "error",
        });
      });
  }

  componentDidMount() {
    this.getCollection();
  }

  render() {
    return (
      <InvitePage
        handleInput={this.handleInput}
        handleInviteForm={this.handleInviteForm}
        resendInvite={this.resendInvite}
        users={this.state.users}
      />
    );
  }
}
