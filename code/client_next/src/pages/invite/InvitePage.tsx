import React from "react";
import {
  Button,
  TextField,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Typography,
  Container,
  CardContent,
  Card,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Box,
} from "@material-ui/core";
import Add from "@material-ui/icons/Add";

export default function InvitePage(props) {
  const { handleInviteForm, handleInput, resendInvite, users } = props;

  return (
    <Box>
      <Container maxWidth="sm">
        <Box marginTop="20px">
          <Accordion>
            <AccordionSummary
              expandIcon={<Add />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography variant="h5">New user</Typography>
            </AccordionSummary>
            <Box display="flex" justifyContent="center" alignItems="center">
              <AccordionDetails>
                <form method="POST" onSubmit={handleInviteForm}>
                  <Grid container spacing={1} alignItems="center">
                    <Grid item xs={8}>
                      <TextField
                        size="small"
                        variant="outlined"
                        margin="normal"
                        required
                        name="email"
                        label="Email"
                        type="email"
                        id="email"
                        onChange={handleInput}
                      />
                    </Grid>
                    <Grid item xs={1}>
                      <Box marginTop="7px">
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                        >
                          Invite
                        </Button>
                      </Box>
                    </Grid>
                  </Grid>
                </form>
              </AccordionDetails>
            </Box>
          </Accordion>

          <Accordion>
            <AccordionSummary
              expandIcon={<Add />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography variant="h5">Invited users</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Box width="100%">
                <List>
                  {users
                    ? users.map((user) => (
                        <Box p="0.8rem" key={user.email}>
                          <Card>
                            <Box minWidth="350px">
                              <CardContent>
                                <ListItem key={user.email}>
                                  <Box display="flex" alignItems="center">
                                    <ListItemText primary={user.email} />
                                    <ListItemSecondaryAction>
                                      <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={() => resendInvite(user.email)}
                                      >
                                        Resend invitation
                                      </Button>
                                    </ListItemSecondaryAction>
                                  </Box>
                                </ListItem>
                              </CardContent>
                            </Box>
                          </Card>
                        </Box>
                      ))
                    : ""}
                </List>
              </Box>
            </AccordionDetails>
          </Accordion>
        </Box>
      </Container>
    </Box>
  );
}
