import React, { Component } from "react";
import { withRouter } from "next/router";
import axiosReq from "../../api/axiosReq";
import CompleteRecoverPage from "./CompleteRecoverPage";

interface IState {
  isValid: boolean,
  email: string,
  password: string,
  validPassword: boolean
}

class CompleteRecover extends Component<{router}, IState> {
  constructor(props) {
    super(props);

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      isValid: false,
      email: "",
      password: "",
      validPassword: true,
    };
  }

  componentDidMount() {
    axiosReq
      .checkUrl(window.location.pathname, "/api/check-recover")
      .then((res) => {
        this.setState({ email: res.email });
        this.setState({ isValid: true });
      })
      .catch((e) => {
        this.setState({ isValid: false });
        return this.props.router.push("/login");
      });
  }
  handleInput(e) {
    e.target.value.length < 6
      ? this.setState({ validPassword: false, password: e.target.value })
      : this.setState({ validPassword: true, password: e.target.value });
  }
  handleSubmit(e) {
    e.preventDefault();
    axiosReq
      .setPassword(
        this.state.email,
        this.state.password,
        "/api/complete-recover"
      )
      .then((res) => localStorage.setItem("jwt", res.token));
    this.props.router.push("/");
  }

  render() {
    return !this.state.isValid ? (
      <></>
    ) : (
      <CompleteRecoverPage
        handleInput={this.handleInput}
        handleSubmit={this.handleSubmit}
        validPassword={this.state.validPassword}
      />
    );
  }
}

export default withRouter(CompleteRecover);
