import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import { Button, Box, Typography } from "@material-ui/core";

export default class oauthRedirect extends Component<any, any> {
  constructor(props) {
    super(props);

    this.state = {
      linkStatus: "linked",
    };
  }

  componentDidMount() {
    let service = window.location.pathname.includes("google")
      ? "google"
      : "facebook";
    let apiUrl =
      service === "google"
        ? "/api/add-oauth-google"
        : "/api/add-oauth-facebook";
    let params = new URLSearchParams(window.location.search);
    let code = params.get("code");
    axiosReq
      .oauthRedirect(apiUrl, code)
      .then((res) => {
        this.setState({ linkStatus: "successfully linked" });
        localStorage.setItem("jwt", res);
      })
      .catch((e) => {
        this.setState({ linkStatus: "already linked" });
      });
  }
  render() {
    return (
      <Box
        display="flex"
        width="300px"
        height="80vh"
        margin="0 auto"
        alignItems="center"
        textAlign="center"
      >
        <Typography variant="h5">Your account is linked!</Typography>
        <Button
          href="/"
          size="small"
          variant="contained"
          color="secondary"
          type="submit"
        >
          Back
        </Button>
      </Box>
    );
  }
}
