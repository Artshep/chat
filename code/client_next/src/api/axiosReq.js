import axios from "axios";

const privateAxios = axios.create({
  baseURL: "http://localhost:5000",
  headers: {
    "Content-Type": "application/json",
  },
});

privateAxios.interceptors.request.use((config) => {
  config.headers["x-authorization"] = localStorage.getItem("jwt");
  return config;
});

const publicAxios = axios.create({
  baseURL: "http://localhost:5000",
  headers: { "Content-Type": "application/json" },
});

const axiosReq = {
  async login(email, password) {
    const res = await publicAxios.post("/api/login", {
      email: email,
      password: password,
    });
    return await res.data;
  },
  async invite(email) {
    const res = await privateAxios.post("/api/invite", {
      email: email,
    });
    return await res.data;
  },
  async forgotPassword(email) {
    const res = await publicAxios.post("/api/forgot-password", {
      email: email,
    });
    return await res.data;
  },
  async checkUrl(hash, apiPath) {
    const res = await publicAxios.post(apiPath, {
      hash: hash,
    });
    return await res.data;
  },
  async setPassword(email, password, apiPath) {
    const res = await publicAxios.post(apiPath, {
      email: email,
      password: password,
    });
    return await res.data;
  },
  async oauthLogin(url, code) {
    const res = await publicAxios.post(url, {
      code: code,
    });
    return await res.data;
  },
  async oauthRedirect(url, code) {
    const res = await privateAxios.post(url, {
      code: code,
    });
    return await res.data;
  },
  async getCollection(collection) {
    const res = await privateAxios.post("/api/get-collection", {
      collection: collection,
    });
    return await res.data;
  },
  async changeStatus(email) {
    const res = await privateAxios.post("/api/change-status", {
      email: email,
    });
    return await res.data;
  },
  async changeName(name) {
    const res = await privateAxios.post("/api/update-user-name", {
      name: name,
    });
    return await res.data;
  },
  async makeDir(name) {
    const res = await privateAxios.post("/api/add-dir", {
      name: name,
    });
    return await res.data;
  },
};
export default axiosReq;