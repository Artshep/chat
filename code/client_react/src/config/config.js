const config = {
    googleRedirectLink: process.env.REACT_APP_GOOGLE_REDIRECT,
    googleLoginLink: process.env.REACT_APP_GOOGLE_LOGIN,
    facebookRedirectLink: process.env.REACT_APP_FACEBOOK_REDIRECT,
    facebookLoginLink: process.env.REACT_APP_FACEBOOK_LOGIN,
}
export default config
