import React, { Component } from "react";
import { Switch } from "react-router-dom";
import Login from "./views/login/Login";
import PrivateRoute from "./HOC/PrivateRoute";
import OauthRedirect from "./views/oauthRedirect/OauthRedirect";
import CompleteInvitation from "./views/completeInvitation/CompleteInvitation";
import CompleteRecover from "./views/completeRecover/CompleteRecover";
import OauthLogin from "./views/oauthLogin/OauthLogin";
import Forget from "./views/forget/Forget";
import Home from "./views/home/Home";
import Profile from "./views/profile/Profile";
import Invite from "./views/invite/Invite";
import Users from "./views/users/Users";
import MyContext from './components/MyContext'
import SnackAlert from './components/snackAlert/SnackAlert'

import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./components/Theme/Theme";
import PublicRoute from "./HOC/PublicRoute";

export default class App extends Component {
  constructor (props){
    super (props)

    this.onChangeOpen = this.onChangeOpen.bind(this)
    this.state = {
      open: false,
      type: '',
      message: ''
    }
  }

  onChangeOpen (result) {
    this.setState({
      open: result.open,
      type: result.type,
      message: result.message
    })
  }


  render() {
    return (
      <>
        <ThemeProvider theme={theme}>
        <MyContext.Provider value={{onChangeOpen:this.onChangeOpen}}>
        <SnackAlert open={this.state.open} onChangeOpen={this.onChangeOpen} type={this.state.type} message={this.state.message}/>
          <Switch>

            <PublicRoute path='/login' component={Login} name='hello world'/>
            <PublicRoute path='/complete-invitation' component={CompleteInvitation}/>
            <PublicRoute path='/forget' component={Forget}/>
            <PublicRoute path='/complete-recover' component={CompleteRecover}/>


            <PublicRoute path='/facebook-login' component={OauthLogin}/>
            <PublicRoute path='/google-login'  component={OauthLogin}/>

            <PrivateRoute path='/google-redirect' component={OauthRedirect}/>
            <PrivateRoute path='/facebook-redirect' component={OauthRedirect}/>

            <PrivateRoute  exact path='/' component={Home}  />
            <PrivateRoute  path='/profile' component={Profile}  />
            <PrivateRoute  path='/users' component={Users}  />
            <PrivateRoute  path='/invite' component={Invite}  />
          </Switch>
          </MyContext.Provider>
        </ThemeProvider>
      </>
    );
}
}
