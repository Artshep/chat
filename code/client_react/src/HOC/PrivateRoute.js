import React from "react";
import { Route, Redirect} from "react-router-dom";

import parseJwt from "../utils/parseJwt";

import Header from "../components/header/Header";

export default function PrivateRoute(props) {
  const token = localStorage.getItem("jwt");
  if (!token) {
    return <Redirect to="/login" />;
  }
  const userData = parseJwt(token);
  return (
    <>
    <Header role={userData.role}/>
    <Route path={props.path} component={() => React.createElement(props.component, userData)}/>
               </>
    
  );
}
