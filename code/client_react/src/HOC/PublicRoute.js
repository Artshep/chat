import React from 'react'
import { Route, Redirect } from 'react-router-dom'

function PublicRoute (props) {
  const token = localStorage.getItem('jwt')
  if (token) {
    return <Redirect to={'/'}/>
  }
  return (
    <div>
         <Route path={props.path} component={props.component} />
    </div>
  )
}

export default PublicRoute