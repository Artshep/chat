import { createMuiTheme } from "@material-ui/core/styles";
import { indigo, lightBlue } from "@material-ui/core/colors";

const Theme = createMuiTheme({
  palette: {
    type: "light",
    primary: indigo,
    secondary: lightBlue,
  },
  typography: {
    fontFamily: "Roboto, Raleway, Arial",
  },
});

export default Theme;
