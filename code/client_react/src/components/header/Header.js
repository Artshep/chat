import React, {Component} from "react";
import { AppBar, Box, IconButton, Toolbar } from "@material-ui/core";
import { ExitToApp, Forum, Menu } from "@material-ui/icons";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import NavBar from '../navBar/NavBar'

class Header extends Component {
  constructor (props){
    super (props)

    this.state = {
      open: false
    }
  }

  onOpen = (e) => {
    this.setState({open: !this.state.open})
  }

  onHome(e) {
    this.props.history.push("/");
  }

  onLogout(e) {
    localStorage.removeItem("jwt");
    localStorage.removeItem("google");
    localStorage.removeItem("facebook");
    this.props.history.push("/login");
  }

  render() {
    return (
      <AppBar position="static">
        <Toolbar>
        <NavBar open={this.state.open} onOpen={this.onOpen} role={this.props.role} />
        <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                onClick={(e) => this.onOpen(e)}
              >
        <Menu />
        </IconButton>
          <Box display="flex" flexGrow={12}>
            <NavLink
              to="/"
              style={{
                color: "white",
                marginRight: 50,
                textDecoration: "none",
              }}
            >
              <IconButton color="inherit" onClick={(e) => this.onHome(e)}>
                <Forum />
                WeChat
              </IconButton>
            </NavLink>
          </Box>

          <Box>
            <IconButton color="inherit" onClick={(e) => this.onLogout(e)}>
              <ExitToApp />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withRouter(Header);