import React from 'react'
import {
    Button,
    TextField,
    Grid,
    Box,
    Typography,
  } from "@material-ui/core";
  

export default function ForgetPage(props) {
    const {handleInput, handleSubmit} = props

    return (
        <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        height="80vh"
      >
        <Typography variant="subtitle2">
          Provide your email to reset password
        </Typography>
        <form method="POST" onSubmit={handleSubmit}>
          <Grid
            container
            spacing={2}
            direction="row"
            justify="center"
            alignItems="center"
          >
            <Grid item xs={9}>
              <TextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                name="email"
                label="Email"
                type="email"
                id="email"
                onChange={handleInput}
              />
            </Grid>
            <Grid item xs={3}>
              <Button variant="contained" color="primary" type="submit">
                OK
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    )
}
