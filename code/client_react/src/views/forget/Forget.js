import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import MyContext from '../../components/MyContext'
import ForgetPage from './ForgetPage'

export default class Forget extends Component {
  static contextType = MyContext
  constructor(props) {
    super(props);

    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      email: "",
    };
  }

  handleInput(e) {
    this.setState({
      email: e.target.value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    axiosReq
      .forget(this.state.email)
      .then((res) => {
        this.context.onChangeOpen({ message: 'Recover link has been sent!', open: true, type: 'success' })
      })
      .catch((e) => {
        this.context.onChangeOpen({ message: e.response.data, open: true, type: 'error' })
      });
  }

  render() {
    return (
      <ForgetPage handleSubmit={this.handleSubmit} handleInput={this.handleInput}/>
    )
  }
}
