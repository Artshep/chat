import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { withRouter } from "react-router";
import axiosReq from "../../api/axiosReq";
import CompleteRecoverPage from './CompleteRecoverPage'


class CompleteRecover extends Component {
  constructor(props) {
    super(props);

    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      isValid: true,
      uuid: "",
      password: "",
      validPassword: true
    };
  }

  componentDidMount() {
    axiosReq
      .checkUrl(window.location.pathname, "/api/complete-recover")
      .then((res) => {
        this.setState({ uuid: res });
      })
      .catch((e) => {
        this.setState({ isValid: false });
      });
  }
  handleInput(e) {
    e.target.value.length < 6
      ? this.setState({ validPassword: false, password: e.target.value })
      : this.setState({ validPassword: true, password: e.target.value });
  }
  handleSubmit(e) {
    e.preventDefault();
    axiosReq.setPassword(
      this.state.uuid,
      this.state.password,
      "/api/recover-password"
    );
    this.props.history.push("/");
  }

  render() {
    if (!this.state.isValid) return <Redirect to="/login" />;
    return (
      <CompleteRecoverPage handleInput={this.handleInput} handleSubmit={this.handleSubmit} validPassword={this.state.validPassword}/>
    );
  }
}

export default withRouter(CompleteRecover)