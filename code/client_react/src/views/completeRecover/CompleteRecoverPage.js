import React from 'react'
import { Button, TextField, Box, Typography } from "@material-ui/core";

export default function CompleteRecoverPage(props) {
    const {handleSubmit, handleInput, validPassword} = props

    return (
        <Box
        display="flex"
        height="80vh"
        margin="0 auto"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
      >
        <Typography variant="subtitle2">Select new password</Typography>
        <form method="POST" onSubmit={handleSubmit}>
          <Box display="flex" justifyContent="center" alignItems="center">
            <TextField
              size="small"
              variant="outlined"
              margin="normal"
              required
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={handleInput}
            />

            <Box 
            padding="7px 0 0 10px"
            >
              <Button 
              variant="contained" 
              color="primary" 
              type="submit"
              disabled={!validPassword}>
                OK
              </Button>
            </Box>
          </Box>
        </form>
      </Box>
    )
}