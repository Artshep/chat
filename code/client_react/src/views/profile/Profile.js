import React, { Component } from "react";
import MyContext from '../../components/MyContext'
import ProfilePage from './ProfilePage'

import axiosReq from "../../api/axiosReq";
import axios from "axios";


export default class Profile extends Component {
  static contextType = MyContext
  constructor(props) {
    super(props);

    this.changeAvatar = this.changeAvatar.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.handleForm = this.handleForm.bind(this)
    this.state = {};
  }


  componentDidMount() {
    axiosReq
    .getUserProfile()
    .then((res) =>{
      this.setState({ logo: res.logo, changedNickname: res.nickname, email: res.email, role: res.role })
    });
  }

  changeAvatar(e) {
    const formData = new FormData();
    formData.append("avatar", e.target.files[0]);
    axios
      .post("/api/change-avatar", formData, {
        baseURL:'http://localhost:5000',
        headers: {
          "Content-Type": "multipart/form-data",
          "x-authorization": localStorage.getItem("jwt"),
        },
      })
      .then((res) => this.setState({ logo: res.data }));
  }

  handleInput(e) {
    if (e.target.name === "nickname") {
      this.setState({ nickname: e.target.value });
    }
  }
  handleForm(e) {
    e.preventDefault();
    axiosReq
      .changeNickname(this.state.nickname)
      .then((res) => {
        this.context.onChangeOpen({ message: 'Nickname has been changed!', open: true, type: 'success' })
      })
    this.setState({ changedNickname: this.state.nickname });
  }

  render() {
    return (
      <>
       <ProfilePage logo={this.state.logo} changeAvatar={this.changeAvatar} changedNickname={this.state.changedNickname} email={this.state.email} role={this.state.role} handleInput={this.handleInput} handleForm={this.handleForm}/>
      </>
    );
  }
}