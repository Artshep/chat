import React from 'react'
import config from '../../config/config'
import {
    Avatar,
    TextField,
    Button,
    Card,
    CardContent,
    Box,
    Grid,
  } from "@material-ui/core";

let google = localStorage.getItem("google");
let facebook = localStorage.getItem("facebook");

export default function ProfilePage(props) {
    const {logo, changeAvatar, changedNickname, email, role, handleInput, handleForm} = props
    return (
        <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        paddingTop="30px"
      >
        
        <Card>
          <CardContent style={{ minWidth: "600px" }}>
            <Box display="flex">
              <Box
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
              >
                 <Avatar
                   src={`http://localhost:5000/${logo}`} 
                  alt={"avatar"}
                  style={{
                    width: "150px",
                    height: "150px",
                    border: "1px solid #d9d7d7",
                    boxShadow: "1px 1px 3px 1px #000000",
                    marginBottom: "20px",
                  }}
                /> 

                <Button component="label" variant="outlined" color={"primary"}>
                  <input
                    type="file"
                    onChange={changeAvatar}
                    hidden
                  />
                  Change avatar
                </Button>
              </Box>
              <Box width='100%' display="flex" flexDirection="column" padding="20px">
                <Box>Email: {email}</Box>
                <Box>Role: {role}</Box>
                <Box pb={2}>Nickname: {changedNickname}</Box>
                <Grid container spacing={1} >
                {!google && (
                  <Grid item md={4} xs={12}>
              <Button variant="contained"
              color="primary" href={config.googleRedirectLink}>LINK GOOGLE</Button></Grid>
                )}
                {!facebook && (
                  <Grid item md={5} >
              <Button variant="contained"
              color="primary" href={config.facebookRedirectLink}>LINK FACEBOOK</Button></Grid>
                )}
                </Grid>
              </Box>
            </Box>

            <Box paddingTop={1}>
              Change nickname: 
              <form method="POST" onSubmit={handleForm}>
                <Box display="flex" alignItems="center">
                  <TextField
                    size="small"
                    variant="outlined"
                    margin="normal"
                    name="nickname"
                    label="Nickname"
                    type="nickname"
                    id="nickname"
                    onChange={handleInput}
                  />

                  <Box paddingLeft={1} marginTop="7px">
                    <Button variant="contained" color="primary" type="submit">
                      Change
                    </Button>
                  </Box>
                </Box>
              </form>
            </Box>
          </CardContent>
        </Card>
      </Box>
    )
}