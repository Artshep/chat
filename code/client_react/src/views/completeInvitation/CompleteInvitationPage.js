import React from 'react'
import {
  TextField,
  Button,
  Container,
  Typography,
  Box,
} from "@material-ui/core";

export default function CompleteInvitationPage(props) {
    const {handleSubmit, handleChangeInput, validPassword} = props

    return (
        <Container maxWidth="sm">
        <Box
          display="flex"
          flexDirection="column"
          height="80vh"
          margin="0 auto"
          justifyContent="center"
          alignItems="center"
        >
          <Box textAlign="center">
            <Typography variant="subtitle2">Select new password</Typography>
          </Box>

          <form method="POST" onSubmit={handleSubmit}>
            <Box display="flex" justifyContent="center" alignItems="center">
              <TextField
                size="small"
                variant="outlined"
                margin="normal"
                required
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={handleChangeInput}
              />
              <Box padding="7px 0 0 5px">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  disabled={!validPassword}
                >
                  OK
                </Button>
              </Box>
            </Box>
          </form>
        </Box>
      </Container>
    )
}