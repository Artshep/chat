import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { withRouter } from "react-router";
import axiosReq from "../../api/axiosReq";
import CompleteInvitationPage from './CompleteInvitationPage'

class CompleteInvitation extends Component {
  constructor(props) {
    super(props);
    this.handleChangeInput = this.handleChangeInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    this.state = {
      password: "",
      uuid: "",
      isValid: true,
      validPassword: true,
    };
  }

  componentDidMount() {
    axiosReq
      .checkUrl(window.location.pathname, "/api/complete-invitation")
      .then((res) => {
        this.setState({ uuid: res });
      })
      .catch((e) => {
        this.setState({ isValid: false });
      });
  }
  handleChangeInput(e) {
    e.target.value.length < 6
      ? this.setState({ validPassword: false, password: e.target.value })
      : this.setState({ validPassword: true, password: e.target.value });
  }

  handleSubmit(e) {
    console.log(this.state.uuid, this.state.password);
    e.preventDefault();
    axiosReq
      .setPassword(
        this.state.uuid,
        this.state.password,
        "/api/invitation-password"
      )
      .then((res) => {
        this.props.history.push("/");
      });
  }

  render() {
    if (!this.state.isValid)
      return <Redirect to="/login" />;
    return (
      <CompleteInvitationPage handleSubmit={this.handleSubmit} handleChangeInput={this.handleChangeInput} validPassword={this.state.validPassword}/>
    );
  }
}


export default withRouter(CompleteInvitation)