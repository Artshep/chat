import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import { Redirect } from "react-router-dom";

let apiUrl = window.location.pathname.includes('google') ? '/api/google-login' : '/api/facebook-login' 

export default class OauthLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
    };
  }
  componentDidMount() {
    let params = new URLSearchParams(window.location.search);
    let code = params.get("code");
    axiosReq.oauthLogin(apiUrl, code).then((res) => {
      localStorage.setItem("jwt", res);
      this.setState({ signedIn: true });
    });
  }
  render() {
    if (this.state.signedIn === true) return <Redirect to="/" />;
    return <></>;
  }
}
