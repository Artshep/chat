import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import MyContext from '../../components/MyContext'
import InvitePage from './InvitePage'

export default class Invite extends Component {
  static contextType = MyContext
  constructor(props) {
    super(props);

    this.email = this.props.email;

    this.resendInvite = this.resendInvite.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.handleInviteForm = this.handleInviteForm.bind(this)
    this.state = {};

  }

  handleInput(e) {
    this.setState({
      inviteEmail: e.target.value,
    });
  }

  resendInvite(email) {
    axiosReq
      .resendInvite(email)
      .then((res) => {
        this.context.onChangeOpen({ message: 'Invitation has been resend!', open: true, type: 'success' })
      })
  }

  handleInviteForm(e) {
    e.preventDefault();
    axiosReq
      .invite(this.state.inviteEmail, false)
      .then((res) => {
        this.context.onChangeOpen({ message: 'User has been invited!', open: true, type: 'success' })
      })
      .catch((e) => {
        this.context.onChangeOpen({ message: 'User is already invited', open: true, type: 'error' })
      });
  }

  componentDidMount() {
    axiosReq
      .getUsers("/api/invited-list")
      .then((res) => this.setState({ users: res }));
  }

  render() {
    return (
      <InvitePage handleInput={this.handleInput} handleInviteForm={this.handleInviteForm} resendInvite={this.resendInvite} users={this.state.users}/>
    );
  }
}