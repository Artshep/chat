import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import MyContext from '../../components/MyContext'
import { withRouter } from "react-router";
import LoginPage from './LoginPage'

class Login extends Component {
  static contextType = MyContext
  constructor(props) {
    super(props);
    
    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      email: "",
      password: "",
      validPassword: true,
    };
  }

  handleSubmit(e) {
    let email = this.state.email;
    let password = this.state.password;

    e.preventDefault();
    axiosReq
      .login(email, password)
      .then((res) => {
        localStorage.setItem("jwt", res);
        this.setState({ auth: true });
        this.props.history.push("/");
      })
      .catch((e) => {
        this.context.onChangeOpen({ message: e.response.data, open: true, type: 'error' })
      });
  }

  handleInput(e) {
    if (e.target.name === "email") {
      this.setState({ email: e.target.value });
    } else {
      e.target.value.length < 6
      ? this.setState({ validPassword: false, password: e.target.value })
      : this.setState({ validPassword: true, password: e.target.value });
    }
  }

  render() {
    return (
      <LoginPage handleSubmit={this.handleSubmit} handleInput={this.handleInput} validPassword={this.state.validPassword} email={this.state.email}/>
    );
  }
}

export default withRouter(Login)