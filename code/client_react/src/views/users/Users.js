import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import { Button, List, ListItem, ListItemText, ListItemAvatar, Avatar, Container, Card, CardContent, Box, Accordion, AccordionDetails, AccordionSummary, Typography, Grid } from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


export default class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    axiosReq
      .getUsers("/api/users-list")
      .then((res) => this.setState({ users: res }))
  }

  changeStatus (email) {
    axiosReq.changeStatus(email)
    .then ((res) => this.setState ({users: res}))
  }

  render() {
    return (
      <Container maxWidth='sm'>
        <Box
        paddingTop='20px'
        >
                    <Accordion>
              <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography variant="h5">Users</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Box 
                width='100%'
                >
          <List>
            {this.state.users
              ? this.state.users.map((user) => (
                <Box
                >
                <Card >
                <CardContent>
                  <ListItem key={user.email}>
                    <Grid container spacing={1}>
                      <Grid item md={2}>
                    <ListItemAvatar>
                    <Avatar alt="user" src={`http://localhost:5000/${user.logo}`} />
                    </ListItemAvatar>
                    </Grid>
                    <Grid item md={7}>
                      <Box display='flex' flexDirection='column'>
                    <ListItemText>{user.email}</ListItemText>
                    <ListItemText>{user.nickname}</ListItemText>
                    </Box>
                    </Grid>
                    <Grid item md={2}>
                {user.status === 'active' ? <Button variant="contained"
                        color="primary"
                        onClick={(e) => this.changeStatus(user.email)}>Active</Button> :
                        <Button variant="contained"
                        color="secondary"
                        onClick={(e) => this.changeStatus(user.email)}>Blocked</Button> }
              </Grid>
              </Grid>
                  </ListItem>
                  </CardContent>
            </Card>
            </Box>
                ))
              : ""}
          </List>
          </Box>
          </AccordionDetails>
          </Accordion>
          </Box>
      </Container>
    );
  }
}
