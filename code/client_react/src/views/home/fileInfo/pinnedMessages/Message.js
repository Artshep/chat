import React from "react";
import {
  ListItem,
  ListItemText,
  Box,
  Card,
  CardContent,
  ListItemAvatar,
  Avatar,
  Typography,
} from "@material-ui/core";


export default function Message(props) {
  const { message } = props;

  return (
    <Box display="flex" data-id={message.id}>
      <ListItem
      >
          <Box display="flex" >
            <Card>
              <CardContent>
                <Box
                  display="flex"
                  alignItems="center"
                >
                    <ListItemAvatar>
                      <Avatar alt="avatar" src={`http://localhost:5000/${message.logo}`} />
                    </ListItemAvatar>
                  <Box display="flex" flexDirection="column">
                    <ListItemText primary={message.data} />
                      <Box
                        display="flex"
                        justifyContent="space-between"
                      >
                        <Box pr={1}>
                            <Box><Typography color="textSecondary">{message.nickname} </Typography></Box>
                        </Box>
                        <Typography color="textSecondary">{message.date.replace(/:\d+ /, " ")}</Typography>
                      </Box>
                  </Box>
                </Box>
              </CardContent>
            </Card>
          </Box>
      </ListItem>
    </Box>
  );
}