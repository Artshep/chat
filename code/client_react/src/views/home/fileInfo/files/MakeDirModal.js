import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios'

export default function MakeDir(props) {
  const [dirName, changeDirName] = useState()

  const handleMakeDir  = (e) => {
    axios
    .post("/api/make-dir", {dirName, path: props.path.join('/')}, {
      baseURL:'http://localhost:5000/',
      headers: {
        "x-authorization": localStorage.getItem("jwt"),
      }
    })
    .then ((res) => {
      props.setFileTree(res.data)
      props.toggleModal()
    })
  }
  return (
    <>
      <Button variant="outlined" color="primary" onClick={props.toggleModal}>
              <input hidden
              />
              + DIR
      </Button>
      <Dialog open={props.open} onClose={props.toggleModal} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">New Directory</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Choose the name of new directory
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Directory"
            type="text"
            fullWidth
            onChange={(e) =>changeDirName(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.toggleModal} color="primary">
            Cancel
          </Button>
          <Button onClick={handleMakeDir} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}