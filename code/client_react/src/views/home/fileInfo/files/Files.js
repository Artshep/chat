import React, {Component} from "react";
import axios from "axios";
import FilesContent from './FilesContent'

class Files extends Component {
  constructor(props) {
    super(props);

    this.handleMoveTo = this.handleMoveTo.bind(this)
    this.setFileTree = this.setFileTree.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.state = {
      path: [],
      fileTree: null,
      open: false
    };
  }

  componentDidMount() {
    this.getFileTree();
  }

  uploadFile = (e) => {
    const formData = new FormData()
    formData.append("file", e.target.files[0])

    axios.post("/api/upload-file", formData, {
      baseURL:'http://localhost:5000/',
      headers: {
        "Content-Type": "multipart/form-data",
        "x-authorization": localStorage.getItem("jwt"),
        'name': e.target.files[0].name,
        'fileurl': this.state.path.join('/')
      },
    })
    .then((res) => {
      this.getFileTree()
    })
    .catch((error) => console.log('error uploading the file'));
  }


  setFileTree = (fileTree) => this.setState({ fileTree });

  getFileTree() {
    axios
      .get("/api/get-files", {
        baseURL:'http://localhost:5000/',
        headers: { "x-authorization": localStorage.getItem("jwt") },
      })
      .then((res) => {
        this.setState({ fileTree: res.data });
      })
      .catch((error) => console.log('Error getting files from server'));
  }


  buildFilesList(fileTree) {
    const filesList = {};

    for (let key in fileTree) {
      if (typeof fileTree[key] === "object") {
        filesList[key] = "dir";
      } else {
        filesList[key] = "file";
      }
    }
    return filesList;
  }

  setPath = (path) => {
    this.setState((state) => ({ path: [...state.path, path] }));
  };

  getDirName = (path, fileTree) => {
    let result = fileTree;
    if (path[0]) {
      for (let i = 0; i < path.length; i++) {
        result = result[path[i]];
      }
      return result;
    } else return result;
  };

  toggleModal (e) {
    this.setState({open: !this.state.open})
  }

  handleMoveTo(index) {
    this.setState(state => ({path: state.path.slice(0, index)}))
  }


  render() {
    const dirName = this.getDirName(this.state.path, this.state.fileTree);
    const filesList = this.buildFilesList(dirName);

    return (
      <FilesContent handleMoveTo={this.handleMoveTo} open={this.state.open} path={this.state.path} setPath={this.setPath} setFileTree={this.setFileTree} filesList={filesList} uploadFile={this.uploadFile} toggleModal={this.toggleModal}/>
    );
  }
}

export default Files;
