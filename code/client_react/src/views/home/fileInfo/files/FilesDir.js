import React from 'react'
import FolderOpenRoundedIcon from '@material-ui/icons/FolderOpenRounded'
import DescriptionIcon from '@material-ui/icons/Description'
import { Box, Fab, Typography } from '@material-ui/core'


const FilesDir = (props) => {
  let files = []
  let path = props.path.join('/')

  for(let key in props.filesList) {
    let file 
    if(props.filesList[key] === 'dir'){
      file = <Box textAlign='center' width='90px' display='inline' key={key}>
        <Fab  size='small' color='primary' onClick={() => props.setPath(key)}>
          <FolderOpenRoundedIcon/>
        </Fab> 
        <Typography variant='subtitle2'>
          {key}
        </Typography>
      </Box> 
    } else {
      file = <Box display='inline'  width='90px' overflow='hidden'  textAlign='center' key={key}>
        <a href={`http://localhost:5000/front/files/${path}/${key}`} >
          <Fab size='small' color='primary'>
            <DescriptionIcon/>
          </Fab>
        </a>
        <Typography variant='subtitle2'>
            {key}
         </Typography>
      </Box>
    }
    files.push(file)
  }
    

  return (
    <Box display='flex' width='100%' flexWrap='wrap' alignItems='center' >
      {files || null}
    </Box>
  )
  
}
export default FilesDir
