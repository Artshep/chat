import React, {Component} from 'react'
import { Box, Fab, Breadcrumbs, Card } from "@material-ui/core";
import BackupIcon from "@material-ui/icons/Backup";
import FilesDir from "./FilesDir";
import Crumb from "./Crumb";
import MakeDirModal from "./MakeDirModal";

export default class FilesContent extends Component {
    render() {

            const breadCrumb = this.props.path.map((path, index) => {
                return (
                  <Crumb
                    key={index}
                    path={path}
                    handleMoveTo={() => this.props.handleMoveTo(index + 1)}
                  />
                );
              });
        return (
                <>
                <Card>
                  <Box p={1} display="flex" justifyContent="space-between">
                    <Breadcrumbs maxItems={3} aria-label="breadcrumb">
                      <Crumb path="/home" handleMoveTo={() => this.props.handleMoveTo(0)} />
                      {breadCrumb}
                    </Breadcrumbs>
                    <MakeDirModal
                      open={this.props.open}
                      toggleModal={this.props.toggleModal}
                      path={this.props.path}
                      setFileTree={this.props.setFileTree}
                    />
                  </Box>
                </Card>
        
                <Box display="flex" m={2}>
                  <FilesDir
                    filesList={this.props.filesList}
                    setPath={this.props.setPath}
                    path={this.props.path}
                    display="flex"
                    flexWrap="wrap"
                  />
                  <Box display="flex">
                    <label htmlFor="uploadFile">
                      <Fab
                        size="small"
                        color="primary"
                        aria-label="add"
                        component="div"
                      >
                        <BackupIcon />
                      </Fab>
                      <input
                        id="uploadFile"
                        type="file"
                        onChange={(e) => this.props.uploadFile(e)}
                        hidden
                      />
                    </label>
                  </Box>
                </Box>
              </>
        
        )
    }
}
