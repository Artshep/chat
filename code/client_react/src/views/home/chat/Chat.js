import React, { Component } from "react";
import { List, Grid, Box, TextField, IconButton } from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import axiosReq from "../../../api/axiosReq";
import Message from "./Message";

export default class Chat extends Component {
  constructor(props) {
    super(props);

    this.socket = this.props.socket
    this.onDeleteMessage = this.onDeleteMessage.bind(this)
    this.onPinMessage = this.onPinMessage.bind(this)
    this.state = {
      user: {},
      messages: [],
      messageList: [],
    };
  }

  onPinMessage (id) {
    this.socket.emit('pinMessage', id)
}

  onDeleteMessage(id) {
    this.socket.emit('deleteMessage', id)
  }

  componentDidMount() {
    let user = this.props.user;

    this.socket.on("displayMessages", (messages) => {
      this.setState({ messages: messages });
      let messageList = messages.map((message) => {
        let flexDir = message.email === user.email ? "row-reverse" : "row";
        return (
          <Message
            message={message}
            user={user}
            flexDir={flexDir}
            key={message.id}
            onPinMessage={this.onPinMessage}
            onDeleteMessage={this.onDeleteMessage}
          />
        );
      });
      this.setState({ messageList: messageList });
    });
    axiosReq
      .getUserProfile()
      .then((res) =>
        this.setState({ user: { nickname: res.nickname, logo: res.logo } })
      );

    this.socket.on("update", (message) => {
      let flexDir =
        message.email === this.props.user.email ? "row-reverse" : "row";
      let newMessage = (
        <Message
          message={message}
          user={user}
          flexDir={flexDir}
          key={message.id}
          onPinMessage={this.onPinMessage}
          onDeleteMessage={this.onDeleteMessage}
        />
      );
      this.setState({
        messages: [...this.state.messages, message],
        messageList: [...this.state.messageList, newMessage],
      });
    });
  }

  componentWillUnmount() {
    this.socket.close();
  }

  handleInput(e) {
    e.preventDefault();
    this.setState({ messageText: e.target.value });
  }
  handleSubmit(e) {
    e.preventDefault();
    e.target[0].value = "";
    if (!this.state.messageText){
      return
    }
    this.socket.emit("message", {
      logo: this.state.user.logo,
      nickname: this.state.user.nickname,
      email: this.props.user.email,
      date: new Date().toLocaleTimeString(),
      data: this.state.messageText,
    });
    this.setState({messageText: null})
  }

  render() {
    return (
      <Box display="flex" flexDirection="column">
        <List
          style={{
            width: "100%",
            maxHeight: "calc(100vh - 64px - 96px)",
            overflow: "auto",
          }}
        >
          {this.state.messageList}
        </List>
        <form onSubmit={(e) => this.handleSubmit(e)}>
          <Grid container style={{ paddingLeft: "20px" }}>
            <Grid item xs={11}>
              <TextField
                name="msg"
                label="Type message..."
                fullWidth
                onChange={(e) => this.handleInput(e)}
              />
            </Grid>
            <Grid item xs={1} align="right">
              <Box margin="10px 20px 0 0">
                <IconButton aria-label="send" type="submit">
                  <SendIcon />
                </IconButton>
              </Box>
            </Grid>
          </Grid>
        </form>
      </Box>
    );
  }
}
