import React, { useState } from "react";
import {
  ListItem,
  ListItemText,
  Box,
  Card,
  CardContent,
  ListItemAvatar,
  Avatar,
  Typography,
} from "@material-ui/core";
import MessageMenu from "./MessageMenu";

export default function Message(props) {
  const { message, user, flexDir, onPinMessage, onDeleteMessage } = props;
  const [displayMenu, toggleMenu] = useState(false);

  return (
    <Box display="flex" data-id={message.id}>
      <ListItem
        style={{
          flexDirection: flexDir,
        }}
      >
        <MessageMenu onPinMessage={onPinMessage} onDeleteMessage={onDeleteMessage} id={message.id}>
          <Box display="flex" >
            <Card>
              <CardContent>
                <Box
                  display="flex"
                  alignItems="center"
                  onClick={(e) => toggleMenu(!displayMenu)}
                >
                  {user.email !== message.email && (
                    <ListItemAvatar>
                      <Avatar alt="avatar" src={`http://localhost:5000/${message.logo}`} />
                    </ListItemAvatar>
                  )}
                  <Box display="flex" flexDirection="column">
                    <ListItemText primary={message.data} />
                      <Box
                        display="flex"
                        justifyContent="space-between"
                      >
                        <Box pr={1}>
                          {user.email !== message.email && (
                            <Box><Typography color="textSecondary">{message.nickname} </Typography></Box>
                          )}
                        </Box>
                        <Typography color="textSecondary">{message.date.replace(/:\d+ /, " ")}</Typography>
                      </Box>
                  </Box>
                </Box>
              </CardContent>
            </Card>
          </Box>
        </MessageMenu>
      </ListItem>
    </Box>
  );
}