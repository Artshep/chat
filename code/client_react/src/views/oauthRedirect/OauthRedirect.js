import React, { Component } from "react";
import axiosReq from "../../api/axiosReq";
import { Button, Box, Typography } from "@material-ui/core";

let service = window.location.pathname.includes('google') ? 'google' : 'facebook'
let apiUrl = service === 'google' ? '/api/google-redirect' : '/api/facebook-redirect' 


export default class oauthRedirect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      linkStatus: "linked",
    };
  }

  componentDidMount() {
    let params = new URLSearchParams(window.location.search);
    let code = params.get("code");
    axiosReq
      .oauthRedirect(apiUrl, code)
      .then((res) => {
        this.setState({ linkStatus: "successfully linked" });
        localStorage.setItem(service, res);
      })
      .catch((e) => {
        this.setState({ linkStatus: "already linked" });
      });
  }
  render() {
    return (
      <Box
        display="flex"
        width="300px"
        height="80vh"
        margin="0 auto"
        alignItems="center"
        textAlign="center"
      >
        <Typography variant='h5'>Your account is linked!</Typography>
        <Button
          href="/"
          size="small"
          variant="contained"
          color="secondary"
          type="submit"
        >
          Back
        </Button>
      </Box>
    );
  }
}
