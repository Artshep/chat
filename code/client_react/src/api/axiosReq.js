import axios from "axios";

const privateAxios = axios.create({
  baseURL:'http://localhost:5000',
  headers: {
    "Content-Type": "application/json"
  },
});

privateAxios.interceptors.request.use(config=>{
  config.headers['x-authorization'] = localStorage.getItem('jwt')
  return config
})

const publicAxios = axios.create({
  baseURL:'http://localhost:5000',
  headers: { "Content-Type": "application/json" },
});

const axiosReq = {
  async login(email, password) {
    const res = await publicAxios.post("/api/login", {
      email: email,
      password: password,
    });
    return await res.data;
  },
  async invite(email) {
    const res = await privateAxios.post("/api/invite", {
      email: email,
    });
    return await res.data;
  },
  async forget(email) {
    const res = await publicAxios.post("/api/forget", {
      email: email,
    });
    return await res.data;
  },
  async checkUrl(url, apiPath) {
    const res = await publicAxios.post(apiPath, {
      url: url,
    });
    return await res.data;
  },
  async setPassword(uuid, password, apiPath) {
    const res = await publicAxios.post(apiPath, {
      uuid: uuid,
      password: password,
    });
    return await res.data;
  },
  async oauthLogin(url, code) {
    const res = await publicAxios.post(url, {
      code: code,
    });
    return await res.data;
  },
  async oauthRedirect(url, code) {
    const res = await privateAxios.post(url, {
      code: code,
    });
    return await res.data;
  },
  async getUsers(apiPath) {
    const res = await privateAxios.get(apiPath);
    return await res.data;
  },
  async resendInvite(email) {
    const res = await privateAxios.post("/api/resend-invite", {
      email: email,
      resend: true,
    });
    return await res.data;
  },
  async changeStatus(email) {
    const res = await privateAxios.post("/api/change-status", {
      email: email,
    });
    return await res.data;
  },
  async getUserProfile() {
    const res = await privateAxios.get("/api/get-user-profile");
    return await res.data;
  },
  async changeNickname(nickname) {
    const res = await privateAxios.post("/api/change-nickname", {
      nickname: nickname,
    });
    return await res.data;
  },
  async makeDir (name) {
    const res = await privateAxios.post('/api/make-dir', {
      name: name
    })
    return await res.data
  }
};
export default axiosReq;
