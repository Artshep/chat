const express = require("express");
const checkLogin = require("../controllers/login");
const checkUser = require("../middleware/chekUser");
const forgotPassword = require("../controllers/forgotPassword");
const completeInvitation = require("../controllers/completeInvitation");
const checkUuid = require("../middleware/checkUuid");
const completeRecover = require("../controllers/completeRecover");
const { googleLogin, googleAuth } = require("../controllers/googleAuth");
const { facebookAuth, facebookLogin } = require("../controllers/facebookAuth");
const invite = require("../utils/invite");
const resendInvite = require ('../middleware/resendInvite')
const getUuid = require("../middleware/getUuid");
const getCollection = require("../controllers/getCollection");
const changeStatus = require("../controllers/changeStatus");
const {updateUser, changeAvatar} = require ('../controllers/updateUser')
const getFiles = require ('../controllers/getFiles')
const makeDir = require("../controllers/makeDir");

const router = express.Router();

const multer = require("multer");


const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, `front/files/${req.headers.fileurl}`);
  },
  filename: (req, file, cb) =>{
    cb(null, file.originalname);
  }
});

const uploadAvatar = multer({ dest: "front/avatars/" });
const uploadFile = multer({ storage })


router.post("/login", checkLogin);

router.post("/google-login", googleLogin);
router.post("/facebook-login", facebookLogin);

router.post("/invite", checkUser, invite);
router.post("/resend-invite", resendInvite, invite);

router.post("/check-invitation", getUuid, checkUuid);
router.post("/complete-invitation", completeInvitation); 

router.post("/forgot-password", forgotPassword);
router.post("/check-recover", getUuid, checkUuid);
router.post("/complete-recover", completeRecover);

router.post("/add-oauth-google", checkUser, googleAuth); 
router.post("/add-oauth-facebook", checkUser, facebookAuth);

router.post("/get-collection", getCollection);

router.get('/files', checkUser, getFiles)

router.post("/change-status", changeStatus);
router.post("/update-user", checkUser, uploadAvatar.single("avatar"), changeAvatar);

router.post('/add-file', checkUser, uploadFile.single('file'), function(req,res) {
  res.end()
});
router.post('/add-dir', checkUser, makeDir, getFiles)


module.exports = router;
