require("dotenv").config({ path: `${__dirname}/../.env` });
module.exports = {
  port: process.env.PORT || 5000,
  mongoLink: process.env.MONGO_LINK,
  adminEmail: process.env.EMAIL,
  sgApiKey: process.env.SENDGRID_API_KEY,
  jwtSecretKey: process.env.JWT_SECRET_KEY,
  salt: process.env.SALT,

  googleClientId: process.env.GOOGLE_CLIENT_ID,
  googleKey: process.env.GOOGLE_KEY,
  googleRedirectLink: process.env.GOOGLE_REDIRECT,
  googleLoginLink: process.env.GOOGLE_LOGIN,

  facebookClientId: process.env.FACEBOOK_CLIENT_ID,
  facebookKey: process.env.FACEBOOK_KEY,
  facebookRedirectLink: process.env.FACEBOOK_REDIRECT,
  facebookLoginLink: process.env.FACEBOOK_LOGIN,
};
