const mongoose = require("mongoose");

const oAuthSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  type: String,
  oauthId: String,
});

const OAuth = mongoose.model("OAuth", oAuthSchema);

module.exports = OAuth;
