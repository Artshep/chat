const mongoose = require("mongoose");
const uuid = require("node-uuid");

const invitationSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Types.ObjectId,
    ref: "User",
  },
  uuid: {
    type: String,
    default: () => uuid.v4(),
  },
  expiredAt: {
    type: Date,
    default: () => Date.now() + 1000 * 60 * 60,
  },
});

const Invitation = mongoose.model("Invitation", invitationSchema);

module.exports = Invitation;
