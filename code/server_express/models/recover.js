const mongoose = require("mongoose");
const uuid = require("node-uuid");

const recoverSchema = new mongoose.Schema({
  uuid: {
    type: String,
    default: () => uuid.v4(),
  },
  user_id: {
    type: mongoose.Types.ObjectId,
  },
  expiredAt: {
    type: Date,
    default: () => Date.now() + 60 * 1000 * 60,
  },
});

const Recover = mongoose.model("Recover", recoverSchema);

module.exports = Recover;
