module.exports = async function resendInvite(req, res, next) {
  req.resend = true;
  next();
};
