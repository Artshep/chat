const { response, error } = require("../utils/response");

const Invitation = require("../models/invitation");
const Recover = require("../models/recover");
const User = require("../models/user");

module.exports = async function checkUuid(req, res, next) {
  if (req.url === "/check-invitation") {
    const invitation = await Invitation.findOne({
      uuid: req.uuid,
    });
    if (!invitation) {
      error(res, 400, "no such invitation");
    } else {
      const user = await User.findOne({
        _id: invitation.user_id,
      });
      response(res, 200, { email: user.email }, "application/json");
    }
  } else if (req.url === "/check-recover") {
    const recover = await Recover.findOne({
      uuid: req.uuid,
    });
    if (!recover) {
      error(res, 400, "no such recover");
    } else {
      const user = await User.findOne({
        _id: recover.user_id,
      });
      response(res, 200, { email: user.email }, "application/json");
    }
  }
  next();
};
