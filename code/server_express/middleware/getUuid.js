module.exports = async function getUuid(req, res, next) {
  const url = req.body.url;
  if (url.includes("invitation/")) {
    req.uuid = url.substr(url.indexOf("invitation/") + "invitation/".length);
  } else {
    req.uuid = url.substr(url.indexOf("recover/") + "recover/".length);
  }
  next();
};
