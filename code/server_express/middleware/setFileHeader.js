const setFileHeader = (req, res, next) => {
  res.header({ "Content-Disposition": "attachment" });
  next();
};

module.exports = setFileHeader;
