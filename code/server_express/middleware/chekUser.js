const jwt = require("jsonwebtoken");
const User = require("../models/user");
const { jwtSecretKey } = require("../config/config");

async function checkUser(req, res, next) {
  const token = jwt.verify(req.headers["x-authorization"], jwtSecretKey);
  let user = await User.findOne({
    email: token.email,
    role: token.role,
  });
  if (!user) {
    next(new Error("Invalid token"));
    return;
  } else if (user.status === "blocked") {
    next(new Error("User account is blocked"));
    return;
  }
  req.user = user;
  next();
}
module.exports = checkUser;
