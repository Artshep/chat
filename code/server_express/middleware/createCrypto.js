const crypto = require("crypto");
const util = require("util");
const config = require("../config/config");

module.exports = async function (password) {
  const cryptPromise = util.promisify(crypto.pbkdf2);
  const cryptBuffer = await cryptPromise(
    password,
    config.salt,
    100000,
    64,
    "sha512"
  );
  return cryptBuffer.toString("hex");
};
