const express = require("express");
const path = require("path");
const { port, mongoLink } = require("./config/config");
const mongoose = require("mongoose");
const createAdmin = require("./utils/createAdmin");
const setFileHeader = require("./middleware/setFileHeader");
const webSockets = require("./webSockets");
const routes = require("./routes/routes");

const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

app.options("*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.sendStatus(200);
});
app.use("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.use(express.json());
app.use("/api", routes);
app.use("/client", express.static(path.join(__dirname, "client")));
app.use("/front", setFileHeader, express.static(path.join(__dirname, "front")));

createAdmin();

try {
  mongoose.connect(mongoLink, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
  console.log("Mongo has been connected");
} catch (e) {
  console.log("Failed to connect Mongo");
}

http.listen(port, () => {
  console.log("server listening ", port);
  webSockets(io);
});
