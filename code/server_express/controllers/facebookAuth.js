const {
  facebookClientId,
  facebookKey,
  facebookLoginLink,
  facebookRedirectLink,
} = require("../config/config");
const querystring = require("querystring");
const axios = require("axios");
const { signToken } = require("../utils/token");
const { response, error } = require("../utils/response");

const oAuth = require("../models/oauth");
const User = require("../models/user");

const FACEBOOK_TOKEN_URL = "https://graph.facebook.com/v9.0/oauth/access_token";

async function facebookAuth(req, res) {
  const code = req.body.code;
  const facebookToken = await getFacebookToken(code, facebookRedirectLink);
  const userData = await getFacebookUserData(facebookToken);
  const currentOauth = await oAuth.findOne({
    oauthId: userData.id,
  });

  const user = await User.findOne({
    email: req.user.email,
  });
  if (!currentOauth) {
    const oauth = new oAuth({
      user_id: user._id,
      type: "facebook",
      oauthId: userData.id,
    });
    await oauth.save();
  }
  let token = signToken({
    email: user.email,
    id: user._id,
    name: user.name,
    avatar: user.avatar,
    role: user.role,
    oauth: {
      google: !!googleOauth,
      facebook: true,
    },
  });
  response(res, 200, token);
}

async function getFacebookToken(code, link) {
  const body = {
    redirect_uri: link,
    code: code,
    client_id: facebookClientId,
    client_secret: facebookKey,
  };
  const { data } = await axios({
    url: `${FACEBOOK_TOKEN_URL}?${querystring.encode(body)}`,
    method: "get",
  });
  return data.access_token;
}

async function getFacebookUserData(accessToken) {
  const { data } = await axios({
    url: `https://graph.facebook.com/v9.0/me?fields=email&access_token=${accessToken}`,
    method: "get",
  });
  return data;
}

async function facebookLogin(req, res) {
  const code = req.body.code;
  const facebookToken = await getFacebookToken(code, facebookLoginLink);
  const userData = await getFacebookUserData(facebookToken);
  const oauth = await oAuth.findOne({
    oauthId: userData.id,
  });

  if (!oauth) {
    return error(res, 400, "User is not linked");
  }
  const user = await User.findOne({
    _id: oauth.user_id,
  });
  if (user.status === "blocked") return error(res, 400, "User is blocked");
  const oauthList = await oAuth.find({ user_id: user._id });
  let google;
  let facebook;
  for (let i = 0; i < oauthList.length; i++) {
    if ((google = oauthList[i].type === "google" ? true : false)) break;
  }
  for (let i = 0; i < oauthList.length; i++) {
    if ((facebook = oauthList[i].type === "facebook" ? true : false)) break;
  }
  let token = signToken({
    id: user._id,
    avatar: user.avatar,
    name: user.name,
    role: user.role,
    email: user.email,
    role: user.role,
    oauth: {
      google: google || false,
      facebook: facebook || false,
    },
  });

  console.log("successfully logged in with fb");
  response(res, 200, token);
}

module.exports = { facebookAuth, facebookLogin };
