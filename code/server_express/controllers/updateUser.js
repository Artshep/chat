const User = require("../models/user");
const Oauth = require("../models/oauth");
const { response } = require("../utils/response");
const { signToken } = require("../utils/token");

async function updateUser(req, res, next) {
  if (req.body.name) {
    changeNickname(req, res);
  } else {
    next();
  }
}

async function changeAvatar(req, res) {
  await User.findOneAndUpdate(
    { email: req.user.email },
    { avatar: req.file.path },
    { new: true },
    async (err, data) => {
      const token = await setToken(data);
      response(res, 200, token);
    }
  );
}

async function changeNickname(req, res) {
  await User.findOneAndUpdate(
    { email: req.user.email },
    { name: req.body.name },
    { new: true },
    async (err, data) => {
      const token = await setToken(data);
      response(res, 200, token);
    }
  );
}

async function setToken(user) {
  const oauth = await Oauth.find({ user_id: user._id });
  let google;
  let facebook;
  for (let i = 0; i < oauth.length; i++) {
    if ((google = oauth[i].type === "google" ? true : false)) break;
  }
  for (let i = 0; i < oauth.length; i++) {
    if ((facebook = oauth[i].type === "facebook" ? true : false)) break;
  }
  return await signToken({
    email: user.email,
    avatar: user.avatar,
    name: user.name,
    id: user.id,
    role: user.role,
    oauth: {
      google: google || false,
      facebook: facebook || false,
    },
  });
}

module.exports = { updateUser, changeAvatar };