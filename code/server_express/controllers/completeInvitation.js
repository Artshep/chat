const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("../config/config");
const { response, error } = require("../utils/response");
const createCrypto = require("../middleware/createCrypto");

const User = require("../models/user");

async function completeInvitation(req, res) {
  const password = await createCrypto(req.body.password);
  const user = await User.findOne(
    {
      email: req.body.email,
    },
    function (err, data) {
      data.password = password;
      data.save();
    }
  );
  let token = jwt.sign(
    {
      email: user.email,
      name: user.name,
      role: user.role,
      id: user._id,
      avatar: user.avatar,
      oauth: {
        google: false,
        facebook: false,
      },
    },
    jwtSecretKey
  );
  response(res, 200, { token: token });
}

module.exports = completeInvitation;
