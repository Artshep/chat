const {
  googleClientId,
  googleKey,
  googleLoginLink,
  googleRedirectLink,
} = require("../config/config");
const axios = require("axios");
const { signToken } = require("../utils/token");

const oAuth = require("../models/oauth");
const User = require("../models/user");
const { response, error } = require("../utils/response");

const GOOGLE_TOKEN = "https://oauth2.googleapis.com/token";

async function googleAuth(req, res) {
  const code = req.body.code;
  const googleToken = await getGoogleToken(code, googleRedirectLink);
  const userData = await getGoogleUserData(googleToken);
  const user = await User.findOne({
    email: req.user.email,
  });

  const currentOauth = await oAuth.findOne({
    oauthId: userData.id,
  });

  if (!currentOauth) {
    const oauth = new oAuth({
      user_id: user._id,
      type: "google",
      oauthId: userData.id,
    });
    await oauth.save();
  }
  const facebookOauth = await oAuth.findOne({
    user_id: user._id,
    type: "facebook",
  });
  let token = signToken({
    email: user.email,
    name: user.name,
    id: user._id,
    avatar: user.avatar,
    role: user.role,
    oauth: {
      google: true,
      facebook: !!facebookOauth,
    },
  });
  response(res, 200, token);
}

async function getGoogleToken(code, link) {
  const { data } = await axios({
    url: GOOGLE_TOKEN,
    method: "post",
    data: {
      code: code,
      client_id: googleClientId,
      client_secret: googleKey,
      redirect_uri: link,
      grant_type: "authorization_code",
    },
  });
  return data.access_token;
}

async function getGoogleUserData(token) {
  const { data } = await axios({
    url: "https://www.googleapis.com/oauth2/v2/userinfo",
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  return data;
}

async function googleLogin(req, res) {
  const code = req.body.code;
  const googleToken = await getGoogleToken(code, googleLoginLink);
  const userData = await getGoogleUserData(googleToken);
  const oauth = await oAuth.findOne({
    oauthId: userData.id,
  });

  if (!oauth) {
    return error(res, 400, "User is not linked");
  }
  const user = await User.findOne({
    _id: oauth.user_id,
  });
  if (user.status === "blocked") return error(res, 400, "User is blocked");
  const oauthList = await oAuth.find({ user_id: user._id });
  let google;
  let facebook;
  for (let i = 0; i < oauthList.length; i++) {
    if ((google = oauthList[i].type === "google" ? true : false)) break;
  }
  for (let i = 0; i < oauthList.length; i++) {
    if ((facebook = oauthList[i].type === "facebook" ? true : false)) break;
  }
  let token = signToken({
    id: user._id,
    avatar: user.avatar,
    name: user.name,
    role: user.role,
    email: user.email,
    role: user.role,
    oauth: {
      google: google || false,
      facebook: facebook || false,
    },
  });

  console.log("successfully logged in with google");
  response(res, 200, token);
}

module.exports = { googleAuth, googleLogin };
