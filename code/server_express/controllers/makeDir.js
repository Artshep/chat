const { response } = require("../utils/response");
const fs = require("fs");
const path = require("path");

module.exports = async function (req, res, next) {
  fs.mkdirSync(path.resolve(__dirname, `../front/files/${req.body.name}`));
  next();
};
