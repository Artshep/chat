const { adminEmail, sgApiKey } = require("../config/config");
const sgMail = require("@sendgrid/mail");
const createCrypto = require ('../middleware/createCrypto')

const User = require("../models/user");
const generatePassword = require("./generatePassword");

module.exports = async function createAdmin() {
  sgMail.setApiKey(sgApiKey);

  const findAdmin = await User.findOne({
    email: adminEmail,
  });
  const password = generatePassword()
  const cryptoPassword = await createCrypto(password);
  if (!findAdmin) {
    const admin = new User({
      email: adminEmail,
      password: cryptoPassword,
      role: "admin",
      nickname: 'admin'
    });
    admin.save();

    const msg = {
      to: adminEmail, // Change to your recipient
      from: adminEmail, // Change to your verified sender
      subject: "Admin registration",
      text: `Admin account has been created.
      Your password is ${password}
      Press the link to complete registration and set new password http://localhost:3000/`,
      html: `<strong>Admin account has been created.</strong><br />
      Your password is <strong>${password}</strong><br />
      Press the link to visit website http://localhost:3000/`,
    };

    await sgMail
    .send(msg)
    .then(() => {
      console.log("Admin has been invited");
    })
    .catch((error) => {
      console.error(error);
    });
  }
};
