const crypto = require("crypto");

function generatePassword() {
  let current_date = new Date().valueOf().toString();
  let random = Math.random().toString();
  const password = crypto
    .createHash("sha1")
    .update(current_date + random)
    .digest("hex");
  return password;
}

module.exports = generatePassword;
