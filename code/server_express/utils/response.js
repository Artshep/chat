function response(res, status, data = "", contentType = "text/html") {
  res.writeHead(status, {
    "Content-Type": contentType,
  });
  res.end(JSON.stringify(data));
}

function error(res, status, message = "") {
  res.statusCode = status;
  res.end(JSON.stringify(message));
}

module.exports = {
  response: response,
  error: error,
};
