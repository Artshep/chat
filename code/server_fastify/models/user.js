const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const userSchema = new Schema({
  email: String,
  password: String,
  role: {
    type: String,
    default: "user",
  },
  name: {
    type: String,
    default: "",
  },
  avatar: {
    type: String,
    default: "",
  },
  status: {
    type: String,
    required: true,
    default: "active",
  },
});

const User = mongoose.model("User", userSchema);

module.exports = User;
