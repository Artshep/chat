const schema = require("../schemas/schema");
const login = require("../controllers/login");
const { googleLogin, googleAuth } = require("../controllers/googleAuth");
const { facebookAuth, facebookLogin } = require("../controllers/facebookAuth");
const checkUser = require("../middleware/chekUser");
const invite = require("../controllers/invite");
const getCollection = require("../controllers/getCollection");
const getUuid = require("../middleware/getUuid");
const checkUuid = require("../middleware/checkUuid");
const completeInvitation = require("../controllers/completeInvitation");
const forgotPassword = require("../controllers/forgotPassword");
const completeRecover = require("../controllers/completeRecover");
const getFiles = require("../controllers/getFiles");
const makeDir = require("../controllers/makeDir");
const changeStatus = require("../controllers/changeStatus");
const { changeName, changeAvatar } = require("../controllers/updateUser");

const routes = [
  {
    method: "POST",
    url: "/api/login",
    handler: login,
    schema: schema.login,
  },
  {
    method: "POST",
    url: "/api/google-login",
    handler: googleLogin,
    schema: schema.socialLogin,
  },
  {
    method: "POST",
    url: "/api/facebook-login",
    handler: facebookLogin,
    schema: schema.socialLogin,
  },
  {
    method: "POST",
    url: "/api/add-oauth-facebook",
    preHandler: checkUser,
    handler: facebookAuth,
    schema: schema.socialAuth,
  },
  {
    method: "POST",
    url: "/api/add-oauth-google",
    preHandler: checkUser,
    handler: googleAuth,
    schema: schema.socialAuth,
  },
  {
    method: "POST",
    url: "/api/invite",
    preHandler: checkUser,
    handler: invite,
    schema: schema.invite,
  },
  {
    method: "POST",
    url: "/api/get-collection",
    handler: getCollection,
    schema: schema.collection,
  },
  {
    method: "POST",
    url: "/api/check-invitation",
    preHandler: getUuid,
    handler: checkUuid,
    schema: schema.checkInvitation,
  },
  {
    method: "POST",
    url: "/api/complete-invitation",
    handler: completeInvitation,
    schema: schema.completeInvitation,
  },
  {
    method: "POST",
    url: "/api/forgot-password",
    handler: forgotPassword,
    schema: schema.forgotPassword,
  },
  {
    method: "POST",
    url: "/api/check-recover",
    preHandler: getUuid,
    handler: checkUuid,
    schema: schema.checkRecover,
  },
  {
    method: "POST",
    url: "/api/complete-recover",
    handler: completeRecover,
    schema: schema.completeRecover,
  },
  {
    method: "POST",
    url: "/api/change-status",
    handler: changeStatus,
    schema: schema.changeStatus,
  },
  {
    method: "POST",
    url: "/api/update-user-avatar",
    preHandler: checkUser,
    handler: changeAvatar,
    schema: schema.changeAvatar,
  },
  {
    method: "POST",
    url: "/api/update-user-name",
    preHandler: checkUser,
    handler: changeName,
    schema: schema.changeName,
  },
];

module.exports = routes;
