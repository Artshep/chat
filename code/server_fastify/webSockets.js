const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("./config/config");

function webSockets(io) {
  let messageList = [];
  let pinnedMessages = [];

  io.use(function (socket, next) {
    if (socket.handshake.query && socket.handshake.query.token) {
      jwt.verify(
        socket.handshake.query.token,
        jwtSecretKey,
        function (err, decoded) {
          if (err) return next(new Error("Authentication error"));
          socket.decoded = decoded;
          next();
        }
      );
    } else {
      next(new Error("Authentication error"));
    }
  }).on("connection", (socket) => {
    socket.on("pinMessage", (id) => {
      pinnedMessage = messageList.find((message) => {
        return message.id === id;
      });
      pinnedMessages.push(pinnedMessage);
      pinnedMessages = Array.from(new Set(pinnedMessages));
      socket.emit("displayPinnedMessages", pinnedMessages);
      socket.broadcast.emit("displayPinnedMessages", pinnedMessages);
    });

    socket.on("deleteMessage", (id) => {
      messageList = messageList.filter((elem, index) => {
        if (elem.id === id) {
          return false;
        } else {
          return true;
        }
      });
      socket.broadcast.emit("displayMessages", messageList);
      socket.emit("displayMessages", messageList);
    });

    socket.on("message", (message) => {
      message.id = messageList.length;
      messageList.push(message);
      socket.emit("update", message);
      socket.broadcast.emit("update", message);
    });

    socket.emit("displayPinnedMessages", pinnedMessages);
    socket.emit("displayMessages", messageList);
  });
}

module.exports = webSockets;