const jwt = require("jsonwebtoken");
const User = require("../models/user");
const { jwtSecretKey } = require("../config/config");

async function checkUser(req, res) {
  const token = jwt.verify(req.headers["x-authorization"], jwtSecretKey);
  let user = await User.findOne({
    email: token.email,
    role: token.role,
  });
  if (!user) {
    new Error("Invalid token");
    return;
  } else if (user.status === "blocked") {
    new Error("User account is blocked");
    return;
  }
  req.user = user;
}
module.exports = checkUser;
