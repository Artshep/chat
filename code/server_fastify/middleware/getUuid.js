module.exports = async function getUuid(req, res) {
  const url = req.body.hash;
  if (url.includes("invitation/")) {
    req.uuid = url.substr(url.indexOf("invitation/") + "invitation/".length);
  } else {
    req.uuid = url.substr(url.indexOf("recover/") + "recover/".length);
  }
};
