const setFileHeader = async (req, res) => {
  res.header({ "Content-Disposition": "attachment" });

};

module.exports = setFileHeader;
