const schema = {
  login: {
    body: {
      type: "object",
      required: ["email", "password"],
      properties: {
        email: { type: "string" },
        password: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  invite: {
    body: {
      type: "object",
      required: ["email"],
      properties: {
        email: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  forgotPassword: {
    body: {
      type: "object",
      required: ["email"],
      properties: {
        email: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  checkInvitation: {
    body: {
      type: "object",
      required: ["hash"],
      properties: {
        hash: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          email: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
  checkRecover: {
    body: {
      type: "object",
      required: ["hash"],
      properties: {
        hash: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          email: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
  completeInvitation: {
    body: {
      type: "object",
      required: ["email", "password"],
      properties: {
        email: { type: "string" },
        password: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  completeRecover: {
    body: {
      type: "object",
      required: ["email", "password"],
      properties: {
        email: { type: "string" },
        password: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
  socialAuth: {
    body: {
      type: "object",
      required: ["code"],
      properties: {
        code: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  socialLogin: {
    body: {
      type: "object",
      required: ["code"],
      properties: {
        code: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  changeStatus: {
    body: {
      type: "object",
      required: ["email"],
      properties: {
        email: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  changeName: {
    body: {
      type: "object",
      required: ["name"],
      properties: {
        name: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          token: { type: "string" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },

  collection: {
    body: {
      type: "object",
      required: ["collection"],
      properties: {
        collection: { type: "string" },
      },
    },
    response: {
      200: {
        description: "Success response",
        type: "object",
        properties: {
          collection: { type: "array" },
        },
      },
      400: {
        description: "Client error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          message: { type: "string" },
        },
      },
    },
  },
};

module.exports = schema;
