const User = require("../models/user");
const Invitation = require("../models/invitation");
const { response, error } = require("../utils/response");

async function usersList(req, res) {
  if (req.body.collection === "users") {
    const users = await User.find().select('-password -__v');
    response(res, 200, { users: users });
  } else {
    const invitations = await Invitation.find();
    let users = [];
    for (let i = 0; i < invitations.length; i++) {
      let user = await User.findOne({
        _id: invitations[i].user_id,
      }).select('-password -__v');
      users.push(user);
    }
    response(res, 200, { users: users });
  }
}

module.exports = usersList;
