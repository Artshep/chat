const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("../config/config");
const { response, error } = require("../utils/response");
const createCrypto = require("../middleware/createCrypto");

const User = require("../models/user");
const Recover = require ('../models/recover')
const Oauth = require("../models/oauth");

async function completeRecover(req, res) {
  const password = await createCrypto(req.body.password);
  const user = await User.findOne(
    {
      email: req.body.email,
    },
    function (err, data) {
      data.password = password;
      data.save();
    }
  );
  await Recover.findOneAndDelete({user_id: user._id})

  const oauth = await Oauth.find({ user_id: user._id });
  let google;
  let facebook;
  for (let i = 0; i < oauth.length; i++) {
    if ((google = oauth[i].type === "google" ? true : false)) break;
  }
  for (let i = 0; i < oauth.length; i++) {
    if ((facebook = oauth[i].type === "facebook" ? true : false)) break;
  }
  let token = jwt.sign(
    {
      email: user.email,
      name: user.name,
      role: user.role,
      id: user._id,
      avatar: user.avatar,
      oauth: {
        google: google || false,
        facebook: facebook || false,
      },
    },
    jwtSecretKey
  );
  console.log("password has been changed");
  response(res, 200, { token: token });
}

module.exports = completeRecover;
