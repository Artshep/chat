const sgMail = require("@sendgrid/mail");
const { adminEmail, sgApiKey } = require("../config/config");
const generatePassword = require("../utils/generatePassword");
const User = require("../models/user");
const Invitation = require("../models/invitation");
const { error, response } = require("../utils/response");
const createCrypto = require("../middleware/createCrypto");

module.exports = async function sendInvite(req, res) {
  sgMail.setApiKey(sgApiKey);
  const receiver = req.body.email;
  const password = generatePassword();
  const cryptoPassword = await createCrypto(password);
  let user = await User.findOne({ email: receiver });
  let invitation;

  user
    ? (invitation = await Invitation.findOne({
        user_id: user._id,
      }))
    : null;

  user
    ? user
    : (user = new User({
        email: receiver,
        password: cryptoPassword,
        name: receiver.substring(0, receiver.indexOf("@")),
      }));

  invitation
    ? invitation
    : (invitation = new Invitation({
        user_id: user._id,
      }));
  await user.save();
  await invitation.save();

  const msg = {
    to: receiver, // Change to your recipient
    from: adminEmail, // Change to your verified sender
    subject: "You`ve received an invitation!",
    text: `Dear user, you were invited to log in.
    Your password is ${password}
    Press the link to complete registration and set new password http://localhost:3000/complete-invitation/${invitation.uuid}`,
    html: `<strong>Dear user, you were invited to log in.</strong><br />
    Your password is <strong>${password}</strong><br />
    Press the link to complete the registration and set new password http://localhost:3000/complete-invitation/${invitation.uuid}`,
  };

  await sgMail
    .send(msg)
    .then(() => {
      response(res, 200, "Invitation sent");
    })
    .catch((error) => {
      error(res, 400, "Error while inviting user");
    });
};
