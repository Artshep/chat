const User = require("../models/user");
const { response, error } = require("../utils/response");

async function changeStatus(req, res) {
  const user = await User.findOne({
    email: req.body.email,
  });

  user.status = user.status === "blocked" ? "active" : "blocked";
  await user.save();

  response(res, 200, { message: "Status has been changed" });
}

module.exports = changeStatus;
