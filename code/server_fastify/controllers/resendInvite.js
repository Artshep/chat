const sgMail = require("@sendgrid/mail");

async function resendInvite(req, res) {
  const user = await User.findOne({
    email: req.body.email,
  });
  const invitation = await Invitation.findOne({
    user_id: user._id,
  });

  const msg = {
    to: receiver, // Change to your recipient
    from: adminEmail, // Change to your verified sender
    subject: "You`ve received an invitation!",
    text: `Dear user, you were invited to log in.
    Your password is ${password}
    Press the link to complete registration and set new password http://localhost:3000/complete-invitation/${invitation.uuid}`,
    html: `<strong>Dear user, you were invited to log in.</strong><br />
    Your password is <strong>${password}</strong><br />
    Press the link to complete the registration and set new password http://localhost:3000/complete-invitation/${invitation.uuid}`,
  };

  await sgMail
    .send(msg)
    .then(() => {
      console.log("Invitation sent");
    })
    .catch((error) => {
      console.error(error);
    });
  res.end();
}
