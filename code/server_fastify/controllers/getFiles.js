const fs = require("fs");
const { response } = require("../utils/response");
const path = require("path");

function getFileDirTree(req, res) {
  try {
    const result = parseDir(path.resolve(__dirname, "../front/files"));

    response(res, 200, result);
  } catch (error) {
    console.log(error);
  }

  function parseDir(pathDir) {
    const arrayDir = fs.readdirSync(pathDir);
    const objDir = arrayDir.reduce((obj, n) => ((obj[n] = n), obj), {});

    for (let file in objDir) {
      const pathFile = path.join(pathDir, file);
      const stats = fs.statSync(pathFile);

      if (stats.isDirectory()) {
        objDir[file] = parseDir(pathFile);
      }
    }

    return objDir;
  }
}

module.exports = getFileDirTree;
