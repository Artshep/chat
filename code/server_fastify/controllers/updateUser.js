const User = require("../models/user");
const FormData = require('form-data')
const axios = require('axios')
const { nanoid } = require('nanoid')
const { response } = require("../utils/response");
const setToken = require("../utils/setToken");

async function changeAvatar(req, res) {
  const avatar = req.raw.files.avatar
  const formData = new FormData()
  formData.append('avatar', avatar.data)
  const path = `front/avatars/${nanoid()}`
  axios
    .post('http://localhost:4000/api/save-avatar', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'File-Path': path,
        ...formData.getHeaders()
      }
    })


  await User.findOneAndUpdate(
    { email: req.user.email },
    { avatar: path },
    { new: true },
    async (err, data) => {
      const token = await setToken(data);
      response(res, 200, token);
    }
  );
}

async function changeName(req, res) {
  await User.findOneAndUpdate(
    { email: req.user.email },
    { name: req.body.name },
    { new: true },
    async (err, data) => {
      const token = await setToken(data);
      response(res, 200, token);
    }
  );
}

module.exports = { changeName, changeAvatar };