const sgMail = require("@sendgrid/mail");
const { adminEmail, sgApiKey } = require("../config/config");
const { response, error } = require("../utils/response");

const User = require("../models/user");
const Recover = require("../models/recover");

async function resetPassword(req, res) {
  const receiver = req.body.email;
  let user = await User.findOne({ email: receiver });

  if (user) {
    const recover = await new Recover({
      user_id: user._id,
    });
    await recover.save();
    sgMail.setApiKey(sgApiKey);
    const msg = {
      to: receiver,
      from: adminEmail,
      subject: "Password reset",
      text: `You can change your password here: http://localhost:3000/complete-recover/${recover.uuid}`,
      html: `You can change your password here: http://localhost:3000/complete-recover/${recover.uuid}`,
    };
    await sgMail
      .send(msg)
      .then(() => {
        response(res, 200, { message: "Reset link sent" });
      })
      .catch((error) => {
        error(res, 400, error);
      });
  } else {
    error(res, 400, { message: "No such user" });
  }
}

module.exports = resetPassword;
