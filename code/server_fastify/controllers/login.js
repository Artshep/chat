const User = require("../models/user");
const Oauth = require("../models/oauth");
const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("../config/config");
const { response, error } = require("../utils/response");
const createCrypto = require("../middleware/createCrypto");

async function checkLogin(req, res) {
  const user = await User.findOne({ email: req.body.email });
  const password = await createCrypto(req.body.password);
  if (user && user.status === "blocked") {
    return error(res, 400, "User account is blocked");
  } else if (user && user.password == password) {
    const oauth = await Oauth.find({ user_id: user._id });
    let google;
    let facebook;
    for (let i = 0; i < oauth.length; i++) {
      if ((google = oauth[i].type === "google" ? true : false)) break;
    }
    for (let i = 0; i < oauth.length; i++) {
      if ((facebook = oauth[i].type === "facebook" ? true : false)) break;
    }

    let token = jwt.sign(
      {
        id: user.id,
        email: user.email,
        avatar: user.avatar,
        role: user.role,
        oauth: {
          google: google || false,
          facebook: facebook || false,
        },
      },
      jwtSecretKey
    );

    response(res, 200, token, "application/json");
  } else {
    return error(res, 400, "Invalid email or password");
  }
}

module.exports = checkLogin;
