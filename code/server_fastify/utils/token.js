const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("../config/config");

function verifyToken(token) {
  return jwt.verify(token, jwtSecretKey);
}

function signToken(payload) {
  return jwt.sign(payload, jwtSecretKey);
}

module.exports = {
  signToken: signToken,
  verifyToken: verifyToken,
};
