const jwt = require("jsonwebtoken");
const { jwtSecretKey } = require("../config/config");
const OAuth = require("../models/oauth");

async function setToken(user) {
    const oauth = await OAuth.find({ user_id: user._id });
    let google;
    let facebook;
    for (let i = 0; i < oauth.length; i++) {
      if ((google = oauth[i].type === "google" ? true : false)) break;
    }
    for (let i = 0; i < oauth.length; i++) {
      if ((facebook = oauth[i].type === "facebook" ? true : false)) break;
    }
    return await jwt.sign({
      email: user.email,
      avatar: user.avatar,
      name: user.name,
      id: user.id,
      role: user.role,
      oauth: {
        google: google || false,
        facebook: facebook || false,
      },
    }, jwtSecretKey);
  }

  module.exports = setToken