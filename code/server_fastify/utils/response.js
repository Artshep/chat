function response(res, status, data = "", contentType = "text/html") {
  res.code(status, {
    "Content-Type": contentType,
  });
  res.send(JSON.stringify(data));
}

function error(res, status, message = "") {
  res.code(status);
  res.send(JSON.stringify(message));
}

module.exports = {
  response: response,
  error: error,
};
