const fastify = require("fastify")({
  logger: true,
});
const fileUpload = require('fastify-file-upload')
const fastifyStatic = require("fastify-static");
const path = require("path");
const middie = require("middie");
const createAdmin = require("./utils/createAdmin");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config/config");
const useRoute = require("./routes/useRoute.js");
const webSockets = require("./webSockets");
const io = require("socket.io")(fastify.server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});


const start = async () => {
  try {
    await fastify.register(middie);

    fastify.register(fastifyStatic, {
      root: path.join(__dirname, "front"),
      prefix: "/front/",
    });
    fastify.use(cors());

    fastify.register(fileUpload)
    useRoute(fastify);

    await mongoose.connect(config.mongoLink, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    createAdmin();
    console.log("connected to MongoDB");
    await fastify.listen(5000, "0.0.0.0");
    webSockets(io);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
