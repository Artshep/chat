const schema = {
    changeAvatar: {
        response: {
          200: {
            description: "Success response",
            type: "object",
            properties: {
              token: { type: "string" },
            },
          },
          400: {
            description: "Client error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          500: {
            description: "Server error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
        },
      },
      
      files: {
        response: {
          200: {
            description: "Success response",
            type: "object",
            properties: {
              fileTree: { type: "object" },
            },
          },
          400: {
            description: "Client error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          500: {
            description: "Server error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
        },
      },

      addFile: {
        response: {
          200: {
            description: "Success response",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          400: {
            description: "Client error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          500: {
            description: "Server error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
        },
      },

      addDir: {
        body: {
          type: "object",
          required: ["name"],
          properties: {
            name: { type: "string" },
          },
        },
        response: {
          200: {
            description: "Success response",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          400: {
            description: "Client error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
          500: {
            description: "Server error",
            type: "object",
            properties: {
              message: { type: "string" },
            },
          },
        },
      },
}

module.exports = schema