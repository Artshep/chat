const fastify = require("fastify")({
    logger: true,
  });
const middie = require('middie')
const fastifyStatic = require('fastify-static')
const path = require('path')
const useRoute = require ('./routes/useRoute')
const fileUpload = require('fastify-file-upload')
const cors = require('fastify-cors')

const start = async () => {
    try {
      await fastify.register(middie)

      fastify.register(cors, {exposedHeaders: 'Content-Disposition'})
  
      fastify.register(fileUpload)
      useRoute(fastify)

      fastify.register(fastifyStatic, {
        root: path.join(__dirname, 'front'),
        setHeaders: function (res, path, stat){
          res.setHeader("Content-Disposition", "attachment");
        },
        prefix: '/front/',
      })
  
      await fastify.listen(4000, '0.0.0.0');
    } catch (err) {
      fastify.log.error(err);
      process.exit(1);
    }
  }
  
  start();