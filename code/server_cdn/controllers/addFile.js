const fs = require('fs')
const path = require('path')

function addFile(req, reply, done) {
  try {
    const file = req.raw.files.file

    const filePath = path.join('./front/files', req.headers.fileurl, file.name)
    console.log(filePath)
    fs.writeFileSync(filePath, file.data)
    done()
  } catch (error) {
    reply.send ({ message: error.message })
  }
}

module.exports = addFile
