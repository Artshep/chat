const fs = require('fs')

function saveAvatar(req, reply) {
  try {
    const avatar = req.raw.files.avatar
    const avatarPath = req.headers['file-path']
    fs.writeFileSync(avatarPath, avatar.data)
    reply.send ({ message: 'successfully saved' })
  } catch (error) {
    reply.send ({ message: error.message })
  }
}
module.exports = saveAvatar