const fs = require('fs')
const path = require('path')    

const deleteFile = async (req, reply) => {
        const dir = path.resolve(__dirname, `../front/files/${req.body.path}`)
        fs.rmdirSync(dir, { recursive: true })
      } 
module.exports = deleteFile