const fs = require("fs");
const path = require("path");

function getFiles(req, reply) {
  try {
    const result = parseDir(path.resolve(__dirname, "../front/files"));
    reply.send(JSON.stringify(result));
  } catch (error) {
    reply.send({message: error.message});
  }

  function parseDir(pathDir) {
    const arrayDir = fs.readdirSync(pathDir);
    const objDir = arrayDir.reduce((obj, n) => ((obj[n] = n), obj), {});

    for (let file in objDir) {
      const pathFile = path.join(pathDir, file);
      const stats = fs.statSync(pathFile);

      if (stats.isDirectory()) {
        objDir[file] = parseDir(pathFile);
      }
    }

    return objDir;
  }
}

module.exports = getFiles;