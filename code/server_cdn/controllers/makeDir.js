const fs = require("fs");
const path = require("path");

module.exports = async function (req, reply) {
  fs.mkdirSync(path.resolve(__dirname, `../front/files/${req.body.name}`));
};
