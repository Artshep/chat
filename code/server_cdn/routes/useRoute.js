const routes = require("./routes");
const swagger = require("fastify-swagger");
const options = require("../config/options");

function useRoute(fastify) {
  fastify.register(swagger, options);
  routes.forEach((route) => {
    fastify.route(route);
  });
}

module.exports = useRoute;
