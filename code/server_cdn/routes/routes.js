const saveAvatar = require ('../controllers/saveAvatar')
const getFiles = require ('../controllers/getFiles')
const addFile = require ('../controllers/addFile')
const makeDir = require ('../controllers/makeDir')
const deleteFile = require ('../controllers/deleteFile')
const schema = require ('../schemas/schema')

const routes = [
      {
        method: "POST",
        url: "/api/save-avatar",
        handler: saveAvatar,
        schema: schema.changeAvatar
      },
      {
        method: "GET",
        url: "/api/files",
        handler: getFiles,
        schema: schema.files
      },
      {
        method: "POST",
        url: "/api/add-file",
        preHandler: addFile,
        handler: getFiles,
        schema: schema.addFile
      },
      {
        method: "POST",
        url: "/api/add-dir",
        preHandler: makeDir,
        handler: getFiles,
        schema: schema.addDir
      },
      {
        method: "POST",
        url: "/api/delete-file",
        preHandler: deleteFile,
        handler: getFiles
      },
]

module.exports = routes