build: 
	docker build -t node ./docker/node/
up:
	docker run -d -v $(PWD)/db:/data/db -p 27017:27017 --name mongodb mongo
	docker run -d -p 3000:3000 -p 5000:5000 -p 4000:4000 -v $(PWD)/code/:/project/ --name=nodecontainer --link mongodb:db node
shell-mongo:
	docker exec -it mongodb bash
shell-node:
	docker exec -it --user="1000" nodecontainer bash
